#!/bin/sh -e

./configure \
    --prefix=/usr \
    --bindir=/usr/bin \
    --sbindir=/usr/bin \
    --libexecdir=/usr/lib \
    --datarootdir=/usr/share \
    --datadir=/usr/share \
    --sysconfdir=/var/etc \
    --localstatedir=/var \
    --includedir=/usr/include \
    --libdir=/usr/lib \
    --enable-nls \
    --disable-db \
    --disable-doc \
    --disable-debug \
    --enable-pie

make
make DESTDIR="${DESTDIR}" SCONFIGDIR=/var/etc/security install

install -dm755 "${DESTDIR}"/var/etc/pam.d
for FILE in other common-account common-auth common-password common-session system-remote-login system-local-login
do
  install -Dm644 "etc/pam.d/${FILE}" "${DESTDIR}/var/etc/pam.d/"
done

chmod +s "${DESTDIR}"/usr/bin/unix_chkpwd

