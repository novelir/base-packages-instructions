#!/bin/sh -e

cmake -B build \
    -GNinja \
    -DCMAKE_INSTALL_PREFIX="/usr" \
    -DCMAKE_INSTALL_BINDIR="bin" \
    -DCMAKE_INSTALL_SBINDIR="bin" \
    -DCMAKE_INSTALL_LIBEXECDIR="lib" \
    -DCMAKE_INSTALL_SYSCONFDIR="var/etc" \
    -DCMAKE_INSTALL_LOCALSTATEDIR="var" \
    -DCMAKE_INSTALL_RUNSTATEDIR="/run" \
    -DCMAKE_INSTALL_LIBDIR="lib" \
    -DCMAKE_INSTALL_INCLUDEDIR="include" \
    -DCMAKE_INSTALL_DATAROOTDIR="share" \
    -DCMAKE_INSTALL_DATADIR="share" \
    -DCMAKE_BUILD_TYPE="Release" \
    -DUSE_SYSTEM_CURL=1 \
    -DBUILD_CPR_TESTS=0 \
    .

cmake --build build
cmake --install build
