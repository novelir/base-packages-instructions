#!/bin/sh -e

install -Dt "${DESTDIR}/usr/share/fonts/${PILE_NAME}"		-m644 *.ttf
install -Dt "${DESTDIR}/usr/share/licenses/${PILE_NAME}"	-m644 LICENSE.txt

