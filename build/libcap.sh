#!/bin/sh -e

sed -i 's/gcc/clang/' Make.Rules

make prefix='/usr' \
     lib='lib' \
     SBINDIR=/usr/bin \
     LIBDIR=/usr/lib \
     RAISE_SETFCAP=no

make prefix='/usr' \
     lib='lib' \
     SBINDIR=/usr/bin \
     LIBDIR=/usr/lib \
     RAISE_SETFCAP='no' \
     DESTDIR="${DESTDIR}" \
     install

mkdir -p "${DESTDIR}"/usr/lib/security
mkdir -p "${DESTDIR}"/var/etc/security

install -v -m755 pam_cap/pam_cap.so "${DESTDIR}"/usr/lib/security
install -v -m644 pam_cap/capability.conf "${DESTDIR}"/var/etc/security

