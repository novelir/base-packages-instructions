#!/bin/sh -e

python setup.py build
python setup.py install \
                --root="${DESTDIR}"/ \
                --optimize=1 \
                --skip-build

cat > "${DESTDIR}"/usr/bin/setup-meson <<EOF
#!/bin/sh -e
exec meson setup \\
  --prefix        /usr \\
  --bindir        bin \\
  --sbindir       bin \\
  --libexecdir    lib \\
  --datadir       share \\
  --sysconfdir    var/etc \\
  --localstatedir var \\
  --includedir    include \\
  --libdir        lib \\
  --buildtype     release \\
  --auto-features auto \\
  --wrap-mode     nodownload \\
  -D              b_lto=true \\
  -D              b_staticpic=true \\
  -D              b_pie=true \\
  "\$@"
EOF
chmod +x "${DESTDIR}"/usr/bin/setup-meson
