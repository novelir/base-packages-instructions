#!/bin/sh -e

setup-meson \
    --prefix=/usr \
    -Dtrust_module=enabled \
    -Dtrust_paths=/var/etc/ca-certificates/trust-source:/usr/share/ca-certificates/trust-source \
    -Dsystemd=enabled \
    -Dbash_completion=disabled \
    -Dgtk_doc=false \
    -Dman=false \
    -Dnls=true \
    . build

meson compile -C build
meson install -C build

ln -sfr "${DESTDIR}/usr/bin/update-ca-trust" "${DESTDIR}/usr/lib/p11-kit/trust-extract-compat"

