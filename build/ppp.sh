#!/bin/sh -e

export CFLAGS="$CFLAGS -D_GNU_SOURCE"
export CXXFLAGS="$CXXFLAGS -D_GNU_SOURCE"
export LDFLAGS="$LDFLAGS -D_GNU_SOURCE"

# Enable active filter,
sed -i "s:^#FILTER=y:FILTER=y:" pppd/Makefile.linux

# Enable ipv6 support.
sed -i "s:^#HAVE_INET6=y:HAVE_INET6=y:" pppd/Makefile.linux

# Enable Microsoft proprietary Callback Control Protocol.
sed -i "s:^#CBCP=y:CBCP=y:" pppd/Makefile.linux

# Disable EAP-TLS
sed -i "s:^USE_EAPTLS=y:#USE_EAPTLS=n:" pppd/Makefile.linux

./configure \
    --prefix=/usr \
    --bindir=/usr/bin \
    --sbindir=/usr/bin \
    --libexecdir=/usr/lib \
    --datarootdir=/usr/share \
    --datadir=/usr/share \
    --sysconfdir=/var/etc \
    --localstatedir=/var \
    --runstatedir=/run \
    --includedir=/usr/include \
    --libdir=/usr/lib

make COPTS="$CFLAGS"
make DESTDIR="${DESTDIR}" install

install -Dm 644 include/net/ppp_defs.h		"${DESTDIR}"/usr/include/net/ppp_defs.h

install -Dm 755 ip-down                   	"${DESTDIR}"/var/etc/ppp/ip-down
install -Dm 755 ip-down.d.dns.sh          	"${DESTDIR}"/var/etc/ppp/ip-down.d/00-dns.sh
install -Dm 755 ip-up                     	"${DESTDIR}"/var/etc/ppp/ip-up
install -Dm 755 ip-up.d.dns.sh            	"${DESTDIR}"/var/etc/ppp/ip-up.d/00-dns.sh
install -Dm 755 ipv6-down                 	"${DESTDIR}"/var/etc/ppp/ipv6-down
install -Dm 755 ipv6-up                   	"${DESTDIR}"/var/etc/ppp/ipv6-up
install -Dm 755 ipv6-up.d.iface-config.sh 	"${DESTDIR}"/var/etc/ppp/ipv6-up.d/00-iface-config.sh
install -Dm 644 options                   	"${DESTDIR}"/var/etc/ppp/options
install -dm 755                           	"${DESTDIR}"/var/etc/ppp/ipv6-down.d

install -d "${DESTDIR}"/var/etc/ppp/peers
