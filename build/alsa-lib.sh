#!/bin/sh -e

patch -p1 < fix-dlo.patch

./configure \
    --prefix=/usr \
    --bindir=/usr/bin \
    --sbindir=/usr/bin \
    --libexecdir=/usr/lib \
    --datarootdir=/usr/share \
    --datadir=/usr/share \
    --sysconfdir=/var/etc \
    --localstatedir=/var \
    --runstatedir=/run \
    --includedir=/usr/include \
    --libdir=/usr/lib \
    --disable-python \
    --disable-static \
    --disable-resmgr \
    --enable-seq \
    --enable-aload \
    --enable-rawmidi \
    --without-debug \
    --without-versioned

make
make DESTDIR="${DESTDIR}" install
