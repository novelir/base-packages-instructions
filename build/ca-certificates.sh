#!/bin/sh -e

# Remove bash dependency
sed -i 's/#!\/bin\/bash/#!\/bin\/sh/' update-ca-trust

# Change default directory
sed -i 's/DEST=\/etc\/ca-certificates\/extracted/DEST=${DEST:-\/etc\/ca-certificates\/extracted}/' update-ca-trust

install -Dt "${DESTDIR}/usr/bin" update-ca-trust


install -d "${DESTDIR}/var/etc/ca-certificates/trust-source/anchors"
install -d "${DESTDIR}/var/etc/ca-certificates/trust-source/blacklist"
install -d "${DESTDIR}/usr/share/ca-certificates/trust-source/anchors"
install -d "${DESTDIR}/usr/share/ca-certificates/trust-source/blacklist"

install -d "${DESTDIR}/var/etc/ssl/certs/edk2"
install -d "${DESTDIR}/var/etc/ssl/certs/java"
install -d "${DESTDIR}/var/etc/ca-certificates/extracted"

# Compatibility links
ln -sfr "${DESTDIR}/var/etc/ca-certificates/extracted/tls-ca-bundle.pem" "${DESTDIR}/var/etc/ssl/cert.pem"
ln -sfr "${DESTDIR}/var/etc/ca-certificates/extracted/tls-ca-bundle.pem" "${DESTDIR}/var/etc/ssl/certs/ca-certificates.crt"


DEST="${DESTDIR}/var/etc/ca-certificates/extracted" "${DESTDIR}/usr/bin/update-ca-trust"

