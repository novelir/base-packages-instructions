#!/bin/sh -e

# Use objcopy from binutils for efi files creation
sed -i "s/objcopy = find_program('objcopy')/objcopy = '\/usr\/bin\/binutils\/objcopy'/" src/boot/efi/meson.build

{
  mkdir _bin
  cat > _bin/rsync <<EOF
#!/bin/sh
echo "SKIPPED : rsync"
EOF
  chmod +x _bin/rsync
  cat > _bin/otool <<EOF
#!/bin/sh
echo "SKIPPED : otool"
EOF
  chmod +x _bin/otool
  export PATH="${PWD}/_bin:${PATH}"
}

setup-meson \
    --prefix=/usr \
    -Dmode=release \
    -Dsplit-usr=false \
    -Dsplit-bin=false \
    -Drootlibdir=/usr/lib \
    -Drootprefix=/usr \
    -Dinitrd=true \
    -Dcompat-mutable-uid-boundaries=false \
    -Dnscd=false \
    -Dutmp=true \
    -Dhibernate=true \
    -Dldconfig=true \
    -Dresolve=true \
    -Defi=true \
    -Dtpm=true \
    -Denvironment-d=true \
    -Dbinfmt=true \
    -Drepart=false \
    -Dcoredump=true \
    -Dpstore=false \
    -Doomd=true \
    -Dlogind=true \
    -Dhostnamed=true \
    -Dlocaled=true \
    -Dmachined=false \
    -Dportabled=false \
    -Duserdb=true \
    -Dhomed=true \
    -Dnetworkd=true \
    -Dtimedated=true \
    -Dtimesyncd=true \
    -Dremote=false \
    -Dcreate-log-dirs=false \
    -Dnss-myhostname=true \
    -Dnss-mymachines=false \
    -Dnss-resolve=true \
    -Dnss-systemd=true \
    -Dfirstboot=true \
    -Drandomseed=true \
    -Dbacklight=true \
    -Dvconsole=true \
    -Dquotacheck=false \
    -Dsysusers=true \
    -Dtmpfiles=true \
    -Dimportd=false \
    -Dhwdb=true \
    -Drfkill=false \
    -Dxdg-autostart=false \
    -Dman=false \
    -Dhtml=false \
    -Dtranslations=true \
    -Ddbuspolicydir=/usr/share/dbus-1/system.d \
    -Drpmmacrosdir=no \
    -Dinstall-sysconfdir=true \
    -Dfallback-hostname=Novelir \
    -Ddefault-hierarchy=unified \
    -Ddefault-net-naming-scheme=latest \
    -Dadm-group=true \
    -Dwheel-group=true \
    -Dgshadow=true \
    -Ddefault-locale=C \
    -Dservice-watchdog=1min \
    -Ddefault-dnssec=no \
    -Ddefault-dns-over-tls=opportunistic \
    -Ddefault-mdns=no \
    -Ddefault-llmnr=no \
    -Ddns-over-tls=openssl \
    -Ddns-servers='1.1.1.1 1.0.0.1 2606:4700:4700::1111 2606:4700:4700::1001' \
    -Dntp-servers='0.pool.ntp.org 1.pool.ntp.org 2.pool.ntp.org 3.pool.ntp.org' \
    -Dseccomp=false \
    -Dselinux=false \
    -Dapparmor=false \
    -Dsmack=false \
    -Dpolkit=false \
    -Dima=false \
    -Dacl=true \
    -Daudit=false \
    -Dblkid=true \
    -Dfdisk=true \
    -Dkmod=true \
    -Dpam=true \
    -Dpwquality=false \
    -Dmicrohttpd=false \
    -Dlibcryptsetup=true \
    -Dlibcurl=true \
    -Didn=false \
    -Dlibidn2=false \
    -Dlibidn=false \
    -Dlibiptc=false \
    -Dqrencode=false \
    -Dgcrypt=false \
    -Dgnutls=false \
    -Dopenssl=true \
    -Dp11kit=false \
    -Dlibfido2=false \
    -Delfutils=false \
    -Dzlib=true \
    -Dbzip2=true \
    -Dxz=true \
    -Dlz4=true \
    -Dzstd=true \
    -Dxkbcommon=true \
    -Dpcre2=false \
    -Dglib=false \
    -Dgnu-efi=true \
    -Defi-cc=gcc \
    -Defi-ld=/usr/bin/binutils/ld.bfd \
    -Dtests=false \
    -Dok-color=magenta \
    -Doss-fuzz=false \
    -Dllvm-fuzz=false \
    -Dkernel-install=true \
    -Danalyze=true \
    . build

meson compile -C build
meson install -C build

# Fix root home
sed -i "s/\/root/\/home\/root/g" "${DESTDIR}/usr/lib/sysusers.d/basic.conf"

# Fix /etc tmpfiles
sed -i "s/\/etc/\/var\/etc/g" \
    -i "s/\.\.\//\.\.\/\.\.\//g" "${DESTDIR}/usr/lib/tmpfiles.d/etc.conf"


cat > "${DESTDIR}"/var/etc/systemd/network/20-ether.network <<EOF
[Match]
Type=ether

[Network]
DHCP=yes
EOF
chmod 644 "${DESTDIR}"/var/etc/systemd/network/20-ether.network


cat > "${DESTDIR}"/var/etc/systemd/network/25-wlan.network <<EOF
[Match]
Type=wlan

[Network]
DHCP=yes
EOF
chmod 644 "${DESTDIR}"/var/etc/systemd/network/25-wlan.network


cat > "${DESTDIR}"/var/etc/systemd/network/30-wwan.network <<EOF
[Match]
Type=wwan

[Network]
DHCP=yes
EOF
chmod 644 "${DESTDIR}"/var/etc/systemd/network/30-wwan.network

