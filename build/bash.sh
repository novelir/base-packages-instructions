#!/bin/sh -e

export CFLAGS="${CFLAGS} -DNON_INTERACTIVE_LOGIN_SHELLS"

for patch in bash51-0??
do
  patch -p0 -i "$patch"
done

./configure \
    --prefix=/usr \
    --bindir=/usr/bin \
    --sbindir=/usr/bin \
    --libexecdir=/usr/lib \
    --datarootdir=/usr/share \
    --datadir=/usr/share \
    --sysconfdir=/var/etc \
    --localstatedir=/var \
    --includedir=/usr/include \
    --libdir=/usr/lib \
    --enable-readline \
    --enable-nls \
    --without-bash-malloc \
    --with-curses \
    --with-installed-readline

make
make DESTDIR="${DESTDIR}" install

