#!/bin/sh -e

sed '/mr_IN/d' -i po/LINGUAS

./configure \
    --prefix=/usr \
    --bindir=/usr/bin \
    --sbindir=/usr/bin \
    --libexecdir=/usr/lib \
    --datarootdir=/usr/share \
    --datadir=/usr/share \
    --sysconfdir=/var/etc \
    --localstatedir=/var \
    --includedir=/usr/include \
    --libdir=/usr/lib \
    --with-python-binary=python3

sed -i -e 's/ -shared / -Wl,-O1,--as-needed\0/g' libtool

make
make DESTDIR="${DESTDIR}" install

install -m755 -d "${DESTDIR}/etc/security/pwquality.conf.d"

