#!/bin/sh -e

export TERM="xterm-256color"

./configure \
    --prefix=/usr \
    --bindir=/usr/bin \
    --sbindir=/usr/bin \
    --libexecdir=/usr/lib \
    --datarootdir=/usr/share \
    --datadir=/usr/share \
    --sysconfdir=/var/etc \
    --localstatedir=/var \
    --runstatedir=/run \
    --includedir=/usr/include \
    --libdir=/usr/lib \
    --cc="${CC:-cc}" \
    --cxx="${CXX:-c++}" \
    --assert-level=0 \
    --disable-static \
    --disable-doc \
    --disable-htmlpages \
    --disable-manpages \
    --disable-podpages \
    --disable-txtpages \
    --disable-gmp \
    --disable-gnutls \
    --disable-libxcb \
    --disable-vdpau \
    --disable-thumb \
    --disable-asm \
    --disable-debug \
    --disable-lto \
    --enable-gpl \
    --enable-version3 \
    --enable-nonfree \
    --enable-shared \
    --enable-gray \
    --enable-libfontconfig \
    --enable-libfreetype \
 --disable-libglslang \
    --enable-libopus \
    --enable-libsnappy \
 --disable-libv4l2 \
    --enable-libvpx \
    --enable-libwebp \
    --enable-libxml2 \
    --enable-openssl \
    --enable-vulkan \
    --enable-libdrm \
    --enable-pic

make
make DESTDIR="${DESTDIR}" install

rm -rf "${DESTDIR}/usr/share/ffmpeg/examples"

