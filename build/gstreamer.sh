#!/bin/sh -e

export CFLAGS="$CFLAGS -fno-lto"
export CXXFLAGS="$CXXFLAGS -fno-lto"
export LDFLAGS="$LDFLAGS -fno-lto"


for PKG in gst-plugins-base gst-plugins-good gst-plugins-ugly gst-plugins-bad
do
  mv $PKG ../$PKG
done

setup-meson \
    --prefix=/usr \
    -Dgst_debug=false \
    -Ddbghelp=disabled \
    -Dintrospection=disabled \
    -Dexamples=disabled \
    -Dtests=disabled \
    -Dbenchmarks=disabled \
    -Dtools=disabled \
    -Ddoc=disabled \
    -Dnls=disabled \
    -Dglib-asserts=disabled \
    -Dglib-checks=disabled \
    -Dextra-checks=disabled \
    . build

meson compile -C build
meson install -C build


cd ..
for plugin in gst-plugins-base gst-plugins-good gst-plugins-ugly gst-plugins-bad
do
  cd "$plugin"

  setup-meson \
      --prefix=/usr \
      -Dqt5=disabled \
      -Dtremor=disabled \
      -Dgtk_doc=disabled \
      -Dcdparanoia=disabled \
      -Dx11=disabled \
      -Dxshm=disabled \
      -Dxvideo=disabled \
      -Ddoc=disabled \
      -Dexamples=disabled \
      -Dtests=disabled \
      -Dtools=disabled \
      -Dintrospection=disabled \
      -Dnls=disabled \
      -Dglib-asserts=disabled \
      -Dglib-checks=disabled \
      . build

  meson compile -C build
  meson install -C build
  cd ..
done

