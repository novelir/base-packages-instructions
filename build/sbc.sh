#!/bin/sh -e

patch -p1 -i fix-build-on-non-x86.patch

./configure \
    --prefix=/usr \
    --bindir=/usr/bin \
    --sbindir=/usr/bin \
    --libexecdir=/usr/lib \
    --datarootdir=/usr/share \
    --datadir=/usr/share \
    --sysconfdir=/var/etc \
    --localstatedir=/var \
    --runstatedir=/run \
    --includedir=/usr/include \
    --libdir=/usr/lib \
    --disable-static \
    --disable-debug \
    --disable-tester

make
make DESTDIR="${DESTDIR}" install

