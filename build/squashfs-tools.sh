#!/bin/sh -e

patch -p1 -i 0001-squashfs-tools-fix-build-failure-against-gcc-10.patch

cd squashfs-tools/

make \
    COMP_DEFAULT="zstd" \
    GZIP_SUPPORT=1 \
    LZMA_XZ_SUPPORT=1 \
    LZ4_SUPPORT=1 \
    ZSTD_SUPPORT=1 \
    XATTR_SUPPORT=1

make INSTALL_DIR="${DESTDIR}/usr/bin" install
