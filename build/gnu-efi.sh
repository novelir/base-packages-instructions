#!/bin/sh -e

patch -p1 -i gnu-efi-3.0.9-fix-clang-build.patch
patch -p1 -i gnu-efi-3.0.10-fallthroug.patch

make CC="${CC}" HOSTCC="${CC}" LD="${LD}"
make PREFIX=/usr INSTALLROOT="${DESTDIR}" install

