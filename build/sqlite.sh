#!/bin/sh -e

export CPPFLAGS="$CPPFLAGS -DSQLITE_ENABLE_COLUMN_METADATA=1"

sed -i 's/ -ltinfo//g' configure

./configure \
    --prefix=/usr \
    --bindir=/usr/bin \
    --sbindir=/usr/bin \
    --libexecdir=/usr/lib \
    --datarootdir=/usr/share \
    --datadir=/usr/share \
    --sysconfdir=/var/etc \
    --localstatedir=/var \
    --runstatedir=/run \
    --includedir=/usr/include \
    --libdir=/usr/lib \
    --disable-debug \
    --enable-threadsafe \
    --enable-dynamic-extensions \
    --enable-fts5

make -j1
make DESTDIR="${DESTDIR}" install

