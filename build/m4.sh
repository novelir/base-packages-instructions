#!/bin/sh -e

sed -i 's/IO_ftrylockfile/IO_EOF_SEEN/' lib/*.c
echo "#define _IO_IN_BACKUP 0x100" >> lib/stdio-impl.h

./configure \
    --prefix=/usr \
    --bindir=/usr/bin \
    --sbindir=/usr/bin \
    --libexecdir=/usr/lib \
    --datarootdir=/usr/share \
    --datadir=/usr/share \
    --sysconfdir=/var/etc \
    --localstatedir=/var \
    --includedir=/usr/include \
    --libdir=/usr/lib

make
make DESTDIR="${DESTDIR}" install
