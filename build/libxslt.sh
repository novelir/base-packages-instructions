#!/bin/sh -e

./configure \
    --prefix=/usr \
    --bindir=/usr/bin \
    --sbindir=/usr/bin \
    --libexecdir=/usr/lib \
    --datarootdir=/usr/share \
    --datadir=/usr/share \
    --sysconfdir=/var/etc \
    --localstatedir=/var \
    --includedir=/usr/include \
    --libdir=/usr/lib \
    --without-debug \
    --without-debugger \
    --without-python \
    --without-profiler

make
make DESTDIR="${DESTDIR}" install

