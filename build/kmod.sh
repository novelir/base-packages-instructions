#!/bin/sh -e

./configure \
    --prefix=/usr \
    --bindir=/usr/bin \
    --sbindir=/usr/bin \
    --libexecdir=/usr/lib \
    --datarootdir=/usr/share \
    --datadir=/usr/share \
    --sysconfdir=/var/etc \
    --localstatedir=/var \
    --includedir=/usr/include \
    --libdir=/usr/lib \
    --disable-manpages \
    --disable-test-modules \
    --with-zstd \
    --with-xz \
    --with-zlib \
    --with-openssl

make
make DESTDIR="${DESTDIR}" install

for bin in depmod insmod lsmod modinfo modprobe rmmod
do
  ln -sf kmod "${DESTDIR}"/bin/$bin
done

