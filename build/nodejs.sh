#!/bin/sh -e

/configure \
    --prefix=/usr \
    --no-cross-compiling \
    --dest-os=linux \
    --ninja \
    --experimental-http-parser \
    --shared-openssl \
    --shared-zlib \
    --without-npm \
    --without-intl \
    --enable-lto

ninja
ninja DESTDIR="${DESTDIR}" install
