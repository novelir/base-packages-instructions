#!/bin/sh -e

sed -i 's/set(COMPILER_RT_HAS_SANITIZER_COMMON TRUE)/set(COMPILER_RT_HAS_SANITIZER_COMMON FALSE)/' compiler-rt/cmake/config-ix.cmake

cmake -B build \
    -GNinja \
    -DCMAKE_INSTALL_PREFIX="/usr" \
    -DCMAKE_INSTALL_BINDIR="bin" \
    -DCMAKE_INSTALL_SBINDIR="bin" \
    -DCMAKE_INSTALL_LIBEXECDIR="lib" \
    -DCMAKE_INSTALL_SYSCONFDIR="var/etc" \
    -DCMAKE_INSTALL_LOCALSTATEDIR="var" \
    -DCMAKE_INSTALL_RUNSTATEDIR="/run" \
    -DCMAKE_INSTALL_LIBDIR="lib" \
    -DCMAKE_INSTALL_INCLUDEDIR="include" \
    -DCMAKE_INSTALL_DATAROOTDIR="share" \
    -DCMAKE_INSTALL_DATADIR="share" \
    -DCMAKE_BUILD_TYPE="Release" \
    -DCMAKE_C_COMPILER='clang' \
    -DCMAKE_CXX_COMPILER='clang++' \
    -DLLVM_HOST_TRIPLE="$(cc -dumpmachine)" \
    -DLLVM_TARGETS_TO_BUILD='host;AMDGPU' \
    -DLLVM_ENABLE_PIC=ON \
    -DLLVM_ENABLE_LTO='Thin' \
    -DLLVM_ENABLE_THREADS=ON \
    -DLLVM_ENABLE_RTTI=ON \
    -DLLVM_ENABLE_EH=ON \
    -DLLVM_ENABLE_PLUGINS=ON \
    -DLLVM_ENABLE_SPHINX=OFF \
    -DLLVM_ENABLE_DOXYGEN=OFF \
    -DLLVM_ENABLE_OCAMLDOC=OFF \
    -DLLVM_ENABLE_UNWIND_TABLES=ON \
    -DLLVM_ENABLE_TERMINFO=OFF \
    -DLLVM_ENABLE_LIBXML2=OFF \
    -DLLVM_ENABLE_LIBEDIT=OFF \
    -DLLVM_ENABLE_PROJECTS='clang;compiler-rt;libcxx;libcxxabi;libunwind;lld;polly' \
    -DLLVM_ENABLE_LIBCXX=ON \
    -DLLVM_ENABLE_LLD=ON \
    -DLLVM_LINK_LLVM_DYLIB=ON \
    -DLLVM_BUILD_LLVM_DYLIB=ON \
    -DLLVM_BUILD_DOCS=OFF \
    -DLLVM_BUILD_EXAMPLES=OFF \
    -DLLVM_BUILD_TESTS=OFF \
    -DLLVM_BUILD_BENCHMARKS=OFF \
    -DLLVM_INCLUDE_DOCS=OFF \
    -DLLVM_INCLUDE_TESTS=OFF \
    -DLLVM_INCLUDE_BENCHMARKS=OFF \
    -DLLVM_INSTALL_BINUTILS_SYMLINKS=ON \
    -DLLVM_INSTALL_CCTOOLS_SYMLINKS=ON \
    -DLIBUNWIND_USE_COMPILER_RT=ON \
    -DLIBUNWIND_INCLUDE_DOCS=OFF \
    -DCLANG_INCLUDE_TESTS=OFF \
    -DCLANG_INCLUDE_DOCS=OFF \
    -DCLANG_DEFAULT_CXX_STDLIB='libc++' \
    -DCLANG_DEFAULT_RTLIB='compiler-rt' \
    -DCOMPILER_RT_INCLUDE_TESTS=OFF \
    -DCOMPILER_RT_USE_BUILTINS_LIBRARY=ON \
    -DCOMPILER_RT_EXCLUDE_ATOMIC_BUILTIN=OFF \
    -DLIBCXX_INCLUDE_BENCHMARKS=OFF \
    -DLIBCXX_INCLUDE_TESTS=OFF \
    -DLIBCXX_INCLUDE_DOCS=OFF \
    -DLIBCXX_HAS_ATOMIC_LIB=OFF \
    -DLIBCXX_USE_COMPILER_RT=ON \
    -DLIBCXX_CXX_ABI='libcxxabi' \
    -DLIBCXXABI_ENABLE_PIC=ON \
    -DLIBCXXABI_USE_LLVM_UNWINDER=ON \
    -DLIBCXXABI_USE_COMPILER_RT=ON \
    -Wno-dev llvm

cmake --build build
cmake --install build


# libc++abi header files
cp -r libcxxabi/include/* "${DESTDIR}"/include/

# LLD: Replace GNU binutils
ln -sf ld.lld "${DESTDIR}"/bin/ld

# Clang: Some legacy programs may require cc
ln -sf clang "${DESTDIR}"/bin/cc

# Clang: equivalent for c++
ln -sf clang++ "${DESTDIR}"/bin/c++

# Clang: equivalent for cpp
ln -sf clang-cpp "${DESTDIR}"/bin/cpp

# Clang: POSIX compliance
cat > c99 <<EOF
#!/bin/sh
exec cc -std=c99 "$@"
EOF
install -Dm755 c99 "${DESTDIR}"/bin/c99

