#!/bin/sh -e

sed -i '/skipping/d' util/packer.c

./configure \
    --prefix=/usr \
    --bindir=/usr/bin \
    --sbindir=/usr/bin \
    --libexecdir=/usr/lib \
    --datarootdir=/usr/share \
    --datadir=/usr/share \
    --sysconfdir=/var/etc \
    --localstatedir=/var \
    --includedir=/usr/include \
    --libdir=/usr/lib \
    --without-python

make
make DESTDIR="${DESTDIR}" install

# Install cracklib-words
{
  bzcat cracklib-words.bz2 > "${DESTDIR}/usr/share/cracklib/cracklib-words"
  chmod 644                  "${DESTDIR}/usr/share/cracklib/cracklib-words"
}

