#!/bin/sh -e

export CFLAGS="$CFLAGS -fno-lto"
export CXXFLAGS="$CXXFLAGS -fno-lto"
export LDFLAGS="$LDFLAGS -fno-lto"

./configure \
    -prefix /usr \
    -bindir /usr/bin/qt5 \
    -headerdir /usr/include/qt5 \
    -archdatadir /usr/lib/qt5 \
    -libexecdir /usr/lib/qt5/libexec \
    -datadir /usr/share/qt5 \
    -docdir /usr/share/doc/qt5 \
    -translationdir /usr/share/qt5/translations \
    -sysconfdir /etc \
    -examplesdir /usr/share/doc/qt5/examples \
    -testsdir /usr/share/doc/qt5/tests \
    -opensource \
    -confirm-license \
    -release \
    -no-gdb-index \
    -platform linux-clang-libc++ \
    -no-reduce-relocations \
    -no-pch \
    -gui \
    -widgets \
    -nomake examples \
    -nomake tests \
    -dbus-linked \
    -qt-doubleconversion \
    -no-glib \
    -icu \
    -qt-pcre \
    -system-zlib \
    -journald \
    -no-syslog \
    -no-slog2 \
    -openssl-linked \
    -no-libproxy \
    -cups \
    -fontconfig \
    -system-freetype \
    -system-harfbuzz \
    -no-gtk \
    -opengl es2 \
    -opengles3 \
    -egl \
    -qpa wayland-egl \
    -eglfs \
    -gbm \
    -kms \
    -no-xcb \
    -libudev \
    -evdev \
    -libinput \
    -mtdev \
    -xkbcommon \
    -system-libpng \
    -system-libjpeg \
    -system-sqlite \
    -no-pulseaudio \
    -alsa \
    -gstreamer 1.0 \
    -skip-qtwebengine \

make
make INSTALL_ROOT="${DESTDIR}" install

