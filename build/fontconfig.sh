#!/bin/sh -e

build_freetype() (
    cd freetype
    rm -rf build
    mkdir build
    cd build
    
    ../configure \
        --prefix=/usr \
        --bindir=/usr/bin \
        --sbindir=/usr/bin \
        --libexecdir=/usr/lib \
        --datarootdir=/usr/share \
        --datadir=/usr/share \
        --sysconfdir=/var/etc \
        --localstatedir=/var \
        --includedir=/usr/include \
        --libdir=/usr/lib \
        --enable-static \
        --enable-freetype-config \
        --with-zlib=yes \
        --with-bzip2=yes \
        --with-png=yes \
        --with-brotli=no \
        --with-harfbuzz="$2"

    make
    make DESTDIR="$1" install
)

build_fontconfig() (
    cd fontconfig
    rm -rf build
    mkdir build
    cd build
    
    rm -f src/fcobjshash.h

    ../configure \
        --prefix=/usr \
        --bindir=/usr/bin \
        --sbindir=/usr/bin \
        --libexecdir=/usr/lib \
        --datarootdir=/usr/share \
        --datadir=/usr/share \
        --sysconfdir=/var/etc \
        --localstatedir=/var \
        --includedir=/usr/include \
        --libdir=/usr/lib \
        --enable-static \
        --disable-docs \
        --enable-nls

    rm -f src/fcobjhash.h

    make
    make DESTDIR="$1" install
)

build_harfbuzz() (
    cd harfbuzz
    rm -rf build

    export DESTDIR="$1"

    setup-meson \
        --prefix=/usr \
        -Ddefault_library=both \
        -Dglib=enabled \
        -Dfreetype=enabled \
        -Dfontconfig=enabled \
        -Dcairo=disabled \
        -Dicu=enabled \
        -Ddocs=disabled \
        -Dintrospection=disabled \
        -Dbenchmark=disabled \
        . build

    meson compile -C build
    meson install -C build
)


# Enable modules
sed -ri "s:.*(AUX_MODULES.*valid):\1:"		freetype/modules.cfg

# Enable subpixel rendering
sed -ri "s:.*(#.*SUBPIXEL_RENDERING) .*:\1:"	freetype/include/freetype/config/ftoption.h


build_freetype		/		no
build_fontconfig	/
build_harfbuzz		/
build_freetype		"${DESTDIR}"	yes
build_fontconfig	"${DESTDIR}"
build_harfbuzz		"${DESTDIR}"


for i in 10-scale-bitmap-fonts.conf 10-autohint.conf 10-hinting-full.conf 10-hinting-medium.conf 10-hinting-none.conf 11-lcdfilter-legacy.conf 11-lcdfilter-light.conf 70-yes-bitmaps.conf
do
  unlink /etc/fonts/conf.d/$i || true
done

for i in 10-hinting-slight.conf 10-sub-pixel-rgb.conf 11-lcdfilter-default.conf 20-unhint-small-vera.conf 70-no-bitmaps.conf
do
  ln -sf /usr/share/fontconfig/conf.avail/$i "${DESTDIR}"/var/etc/fonts/conf.d/$i
done

