#!/bin/sh -e

./configure \
    --prefix=/usr \
    --bindir=/usr/bin \
    --sbindir=/usr/bin \
    --libexecdir=/usr/lib \
    --datarootdir=/usr/share \
    --datadir=/usr/share \
    --sysconfdir=/var/etc \
    --localstatedir=/var \
    --includedir=/usr/include \
    --libdir=/usr/lib \
    --disable-profiling \
    --disable-optimizations \
    --enable-shared \
    --enable-ipv6 \
    --without-doc-strings \
    --with-lto \
    --with-ensurepip=yes \
    --with-system-ffi \
    --with-system-expat \
    --with-system-libmpdec \
    --with-tzpath=/usr/share/zoneinfo

make
make DESTDIR="${DESTDIR}" install

ln -sf python3		"${DESTDIR}"/usr/bin/python
ln -sf python3-config	"${DESTDIR}"/usr/bin/python-config
ln -sf idle3		"${DESTDIR}"/usr/bin/idle
ln -sf pydoc3		"${DESTDIR}"/usr/bin/pydoc
ln -sf pip3		"${DESTDIR}"/usr/bin/pip

