#!/bin/sh -e

export CFLAGS="$CFLAGS -fno-lto"
export CXXFLAGS="$CXXFLAGS -fno-lto"
export LDFLAGS="$LDFLAGS -fno-lto"

./configure \
    --prefix=/usr \
    --bindir=/usr/bin \
    --sbindir=/usr/bin \
    --libexecdir=/usr/lib \
    --datarootdir=/usr/share \
    --datadir=/usr/share \
    --sysconfdir=/var/etc \
    --localstatedir=/var \
    --includedir=/usr/include \
    --libdir=/usr/lib

make
make DESTDIR="${DESTDIR}" install
