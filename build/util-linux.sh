#!/bin/sh -e

./configure \
    --prefix=/usr \
    --bindir=/usr/bin \
    --sbindir=/usr/bin \
    --libexecdir=/usr/lib \
    --datarootdir=/usr/share \
    --datadir=/usr/share \
    --sysconfdir=/var/etc \
    --localstatedir=/var \
    --includedir=/usr/include \
    --libdir=/usr/lib \
    --disable-gtk-doc \
    --disable-symvers \
    --disable-assert \
    --disable-rpath \
    --disable-login \
    --disable-nologin \
    --disable-sulogin \
    --disable-last \
    --disable-su \
    --disable-hardlink \
    --disable-plymouth_support \
    --enable-nls \
    --enable-libuuid-force-uuidd \
    --enable-libmount-support-mtab \
    --without-libmagic \
    --without-selinux \
    --without-audit \
    --without-tinfo \
    --without-cap-ng \
    --without-smack \
    --without-python \
    --with-systemd \
    --with-udev

make
make DESTDIR="${DESTDIR}" install

install -Dm644 runuser.pam "${DESTDIR}/var/etc/pam.d/"

install -dm644 "${DESTDIR}/usr/lib/sysusers.d/"
echo 'u uuidd 68'       > "${DESTDIR}/usr/lib/sysusers.d/util-linux.conf"
echo 'g rfkill - - -'	>> "${DESTDIR}/usr/lib/sysusers.d/util-linux.conf"
chmod 644 "${DESTDIR}/usr/lib/sysusers.d/util-linux.conf"

