#!/bin/sh -e

export FORCE_UNSAFE_CONFIGURE=1

./configure \
    --prefix=/usr \
    --bindir=/usr/bin \
    --sbindir=/usr/bin \
    --libexecdir=/usr/lib \
    --datarootdir=/usr/share \
    --datadir=/usr/share \
    --sysconfdir=/var/etc \
    --localstatedir=/var \
    --includedir=/usr/include \
    --libdir=/usr/lib \
    --disable-rpath \
    --disable-assert \
    --enable-acl \
    --enable-libcap \
    --enable-no-install-program=groups,hostname,kill,uptime \
    --without-gmp \
    --without-selinux \
    --with-openssl

make
make DESTDIR="${DESTDIR}" install
