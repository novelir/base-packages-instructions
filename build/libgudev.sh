#!/bin/sh -e

setup-meson \
    --prefix=/usr \
    -Dtests=disabled \
    -Dvapi=disabled \
    -Dgtk_doc=false \
    . build

meson compile -C build
meson install -C build

