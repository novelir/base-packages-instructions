#!/bin/sh -e

cmake -B build \
    -GNinja \
    -DCMAKE_INSTALL_PREFIX="/usr" \
    -DCMAKE_INSTALL_BINDIR="bin" \
    -DCMAKE_INSTALL_SBINDIR="bin" \
    -DCMAKE_INSTALL_LIBEXECDIR="lib" \
    -DCMAKE_INSTALL_SYSCONFDIR="var/etc" \
    -DCMAKE_INSTALL_LOCALSTATEDIR="var" \
    -DCMAKE_INSTALL_RUNSTATEDIR="/run" \
    -DCMAKE_INSTALL_LIBDIR="lib" \
    -DCMAKE_INSTALL_INCLUDEDIR="include" \
    -DCMAKE_INSTALL_DATAROOTDIR="share" \
    -DCMAKE_INSTALL_DATADIR="share" \
    -DCMAKE_BUILD_TYPE="Release" \
    .

cmake --build build
cmake --install build

install -m644 -D misc/ninja-mode.el "${DESTDIR}/usr/share/emacs/site-lisp/ninja-mode.el"
install -m644 -D misc/ninja.vim "${DESTDIR}/usr/share/vim/vimfiles/syntax/ninja.vim"

install -m644 -D misc/bash-completion "${DESTDIR}/usr/share/bash-completion/completions/ninja"
install -m644 -D misc/zsh-completion "${DESTDIR}/usr/share/zsh/site-functions/_ninja"
