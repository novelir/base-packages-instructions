#!/bin/sh -e

setup-meson \
    --prefix=/usr \
    -Dfreedreno=true \
    -Dtegra=true \
    -Domap=true \
    -Dexynos=true \
    -Dvc4=true \
    -Detnaviv=true \
    -Dudev=true \
    -Dvalgrind=false \
    . build

meson compile -C build
meson install -C build

