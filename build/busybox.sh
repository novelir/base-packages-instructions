#!/bin/sh -e

if [ "${CC:-clang}" = "clang" ]
then
  patch -p1 -i fix-UB+clang.patch

  sed -i 's/gcc/clang/' Makefile
  sed -i 's/g++/clang++/' Makefile
fi

mv busybox.config .config
make oldconfig
make CC="${CC}" HOSTCXX="${CXX}" CXX="${CXX}" HOSTCXX="${CXX}" busybox

mkdir -p "${DESTDIR}"/bin
for bin in $(./busybox --list)
do
  ln -sf busybox "${DESTDIR}"/bin/$bin
done

mv busybox "${DESTDIR}"/bin/busybox
chmod u+s "${DESTDIR}/bin/busybox"

