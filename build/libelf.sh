#!/bin/sh -e

export CFLAGS="-Wno-format-nonliteral -Wno-error -Wno-null-dereference"
export CXXFLAGS="-Wno-format-nonliteral -Wno-error -Wno-null-dereference"
export LDFLAGS="-Wno-format-nonliteral -Wno-error -Wno-null-dereference"


sed -i 's/-Wtrampolines//g' lib/Makefile.in
sed -i 's/-Wtrampolines//g' libelf/Makefile.in
sed -i 's/=5//g'            lib/Makefile.in
sed -i 's/=5//g'            libelf/Makefile.in


# Disable configure error for missing argp.
sed -i 's/as_fn_error.*libargp/: "/g' configure

# Don't compile two unrelated C files which require argp.
sed -i 's/color.*printversion../#/g' lib/Makefile.in


cat > error.h <<EOF
#ifndef _ERROR_H_
#define _ERROR_H_

#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>

static unsigned int error_message_count = 0;

static inline void error(int status, int errnum, const char* format, ...)
{
	va_list ap;
	fprintf(stderr, "%s: ", program_invocation_name);
	va_start(ap, format);
	vfprintf(stderr, format, ap);
	va_end(ap);
	if (errnum)
		fprintf(stderr, ": %s", strerror(errnum));
	fprintf(stderr, "\n");
	error_message_count++;
	if (status)
		exit(status);
}

#endif	/* _ERROR_H_ */
EOF



./configure \
    --prefix=/usr \
    --bindir=/usr/bin \
    --sbindir=/usr/bin \
    --libexecdir=/usr/lib \
    --datarootdir=/usr/share \
    --datadir=/usr/share \
    --sysconfdir=/var/etc \
    --localstatedir=/var \
    --runstatedir=/run \
    --includedir=/usr/include \
    --libdir=/usr/lib \
    --program-prefix=eu- \
    --disable-gprof \
    --disable-gcov \
    --disable-symbol-versioning \
    --disable-debuginfod \
    --disable-libdebuginfod \
    --enable-deterministic-archives \
    ac_cv_c99=yes

# Skip the default make target and build only what we need.
make -C lib
make -C libelf
make -C libelf DESTDIR="${DESTDIR}" install

