#!/bin/sh -e

./configure \
    --prefix=/usr \
     --shared

make
make DESTDIR="${DESTDIR}" install

