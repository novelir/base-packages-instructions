#!/bin/sh -e

export CFLAGS="$CFLAGS -fno-lto"
export CXXFLAGS="$CXXFLAGS -fno-lto"
export LDFLAGS="$LDFLAGS -fno-lto"

./configure \
    -prefix /usr \
    -headerdir /usr/include/qt6 \
    -archdatadir /usr/lib/qt6 \
    -libexecdir /usr/lib/qt6/libexec \
    -datadir /usr/share/qt6 \
    -docdir /usr/share/doc/qt6 \
    -translationdir /usr/share/qt6/translations \
    -sysconfdir /etc \
    -examplesdir /usr/share/doc/qt6/examples \
    -testsdir /usr/share/doc/qt6/tests \
    -cmake-generator Ninja \
    -release \
    -no-gdb-index \
    -platform linux-clang-libc++ \
    -no-reduce-relocations \
    -no-pch \
    -gui \
    -widgets \
    -nomake examples \
    -nomake tests \
    -dbus-linked \
    -qt-doubleconversion \
    -no-glib \
    -icu \
    -qt-pcre \
    -system-zlib \
    -journald \
    -no-syslog \
    -no-slog2 \
    -openssl-linked \
    -no-libproxy \
    -cups \
    -fontconfig \
    -system-freetype \
    -system-harfbuzz \
    -no-gtk \
    -opengl es2 \
    -opengles3 \
    -egl \
    -qpa wayland-egl \
    -eglfs \
    -gbm \
    -kms \
    -no-xcb \
    -libudev \
    -evdev \
    -libinput \
    -mtdev \
    -xkbcommon \
    -system-libpng \
    -system-libjpeg \
    -system-sqlite \
    -- \
    -DINSTALL_MKSPECSDIR=/usr/lib/qt6/mkspecs \
    -S . \
    -B build

cmake --build build
cmake --install build

