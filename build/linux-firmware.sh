#!/bin/sh -e

export MAKE_ARGS="LLVM=1 LLVM_IAS=1"

case $PILE_ARCH in
  aarch64)
    export MAKE_ARGS="$MAKE_ARGS ARCH=arm64"
    ;;
  riscv64)
    export MAKE_ARGS="$MAKE_ARGS ARCH=riscv"
    ;;
  x86_64)
    export MAKE_ARGS="$MAKE_ARGS ARCH=x86_64"
    ;;
esac

make ${MAKE_ARGS} FIRMWAREDIR="/usr/lib/firmware" DESTDIR="${DESTDIR}" install

