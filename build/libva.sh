#!/bin/sh -e

setup-meson \
    --prefix=/usr \
    -Ddocs=false \
    -Dintrospection=false \
    . build

meson compile -C build
meson install -C build

