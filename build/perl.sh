#!/bin/sh -e

export BUILD_ZLIB=False
export BUILD_BZIP2=0

./Configure \
    -Dprefix=/usr \
    -Dvendorprefix=/usr \
    -Dprivlib=/usr/share/perl5/core_perl \
    -Darchlib=/usr/lib/perl5/core_perl \
    -Dscriptdir=/usr/bin \
    -Dsitelib=/usr/share/perl5/site_perl \
    -Dsitearch=/usr/lib/perl5/site_perl \
    -Dsitescript=/usr/bin \
    -Dvendorlib=/usr/share/perl5/vendor_perl \
    -Dvendorarch=/usr/lib/perl5/vendor_perl \
    -Dvendorscript=/usr/bin \
    -des \
    -Dpager="/usr/bin/less -isR" \
    -Duseshrplib \
    -Dusethreads \
    -Dcc="${CC:-cc}" \
    -Dar="${AR:-ar}" \
    -Dnm="${NM:-nm}" \
    -Dranlib="${RANLIB:-ranlib}"

make
make DESTDIR="${DESTDIR}" install

rm -f "${DESTDIR}"/*.0

