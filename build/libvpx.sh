#!/bin/sh -e

# Fix build with busybox diff
sed -e '/diff missing/s/die/echo/' -i configure

# Fix build with clang
unset LD

./configure \
    --prefix=/usr \
    --bindir=/usr/bin \
    --sbindir=/usr/bin \
    --libexecdir=/usr/lib \
    --datarootdir=/usr/share \
    --datadir=/usr/share \
    --sysconfdir=/var/etc \
    --localstatedir=/var \
    --runstatedir=/run \
    --includedir=/usr/include \
    --libdir=/usr/lib \
    --disable-debug \
    --disable-werror \
    --disable-docs \
    --disable-examples \
    --disable-install-docs \
    --disable-install-srcs \
    --enable-pic \
    --enable-postproc \
    --enable-runtime-cpu-detect \
    --enable-shared \
    --enable-vp8 \
    --enable-vp9 \
    --enable-vp9-highbitdepth \
    --enable-vp9-temporal-denoising

make
make DIST_DIR="${DESTDIR}"/usr install
