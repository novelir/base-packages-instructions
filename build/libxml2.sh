#!/bin/sh -e

sed -i 's/ TRUE/ true/' encoding.c

sed -i '/if Py/{s/Py/(Py/;s/)/))/}' python/types.c
sed -i '/if Py/{s/Py/(Py/;s/)/))/}' python/libxml.c

./configure \
    ac_cv_path_PERL="echo" \
    --prefix=/usr \
    --bindir=/usr/bin \
    --sbindir=/usr/bin \
    --libexecdir=/usr/lib \
    --datarootdir=/usr/share \
    --datadir=/usr/share \
    --sysconfdir=/var/etc \
    --localstatedir=/var \
    --includedir=/usr/include \
    --libdir=/usr/lib \
    --without-debug \
    --without-docbook \
    --with-python=/usr/bin/python3 \
    --with-threads \
    --with-history \
    --with-icu \
    --with-lzma \
    --with-zlib

make
make DESTDIR="${DESTDIR}" install

