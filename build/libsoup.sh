#!/bin/sh -e

setup-meson \
    --prefix=/usr \
    -Dintrospection=false \
    -Dgtk-doc=false \
    -Dtests=false \
    -Dvapi=false \
    -Dgnome=false \
    -Dgssapi=false \
    . build

meson compile -C build
meson install -C build

