#!/bin/sh -e

cat > config.mak <<EOF
NO_SVN_TESTS=YesPlease
NO_TCLTK=YesPlease
NO_EXPAT=YesPlease
NO_NSEC=YesPlease
NO_PYTHON=YesPlease
NO_PERL=YesPlease
NO_SVN_TESTS=YesPlease
NO_SYS_POLL_H=1
NO_CROSS_DIRECTORY_HARDLINKS=1
NO_INSTALL_HARDLINKS=1
EOF

./configure \
    --prefix=/usr \
    --bindir=/usr/bin \
    --sbindir=/usr/bin \
    --libexecdir=/usr/lib \
    --datarootdir=/usr/share \
    --datadir=/usr/share \
    --sysconfdir=/var/etc \
    --localstatedir=/var \
    --runstatedir=/run \
    --includedir=/usr/include \
    --libdir=/usr/lib \
    ac_cv_lib_curl_curl_global_init=yes \
    ac_cv_snprintf_returns_bogus=no \
    ac_cv_fread_reads_directories=yes

make LIBS="$(curl-config --static-libs) libgit.a xdiff/lib.a -lz"
make DESTDIR="${DESTDIR}" install

install -dm644 "${DESTDIR}/usr/lib/sysusers.d/"
echo 'u git - "git daemon user" / /usr/bin/git-shell' > "${DESTDIR}/usr/lib/sysusers.d/git.conf"
chmod 644 "${DESTDIR}/usr/lib/sysusers.d/git.conf"

