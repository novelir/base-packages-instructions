#!/bin/sh -e

make

install -Dm755 pigz "${DESTDIR}"/bin/pigz
install -Dm755 unpigz "${DESTDIR}"/bin/unpigz

ln -sf pigz "${DESTDIR}"/bin/gzip
ln -sf pigz "${DESTDIR}"/bin/zcat
ln -sf unpigz "${DESTDIR}"/bin/gunzip
