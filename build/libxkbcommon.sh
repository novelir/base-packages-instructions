#!/bin/sh -e

setup-meson \
    --prefix=/usr \
    -Denable-docs=false \
    -Denable-x11=false \
    -Dxkb-config-root=/usr/share/X11/xkb \
    -Dx-locale-root=/usr/share/X11/locale \
    -Denable-wayland=true \
    . build

meson compile -C build
meson install -C build

