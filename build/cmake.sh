#!/bin/sh -e

for FLAG in '-mllvm -polly' -Wl,--as-needed -Wl,-z,now -Wl,-z,relro
do
  export CFLAGS="$(echo $CFLAGS | sed "s/$FLAG/ /g")"
  export CXXFLAGS="$(echo $CXXFLAGS | sed "s/$FLAG/ /g")"
  export LDFLAGS="$(echo $LDFLAGS | sed "s/$FLAG/ /g")"
done

export CFLAGS="$CFLAGS -O2"
export CXXFLAGS="$CXXFLAGS -O2"
export LDFLAGS="$LDFLAGS -O2"

./bootstrap \
    --prefix=/usr \
    --bindir=/usr/bin \
    --datadir=/usr/share/cmake  \
    --mandir=/usr/share/man \
    --docdir=/usr/share/doc/cmake \
    --system-expat \
    --system-zlib \
    --system-bzip2 \
    --system-liblzma \
    --system-zstd \
    -- \
    -DCMAKE_USE_OPENSSL=OFF \
    -DBUILD_TESTING=OFF

make
make DESTDIR="${DESTDIR}" install
