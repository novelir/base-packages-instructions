#!/bin/sh -e

setup-meson \
    --prefix=/usr \
    -Dintrospection=false \
    -Dgtk-doc=false \
    -Ddemo-agent=false \
    -Dnmea-source=false \
    -D3g-source=true \
    -Dcdma-source=true \
    -Dmodem-gps-source=true \
    . build

meson compile -C build
meson install -C build


install -dm644 "${DESTDIR}/usr/lib/sysusers.d/"
echo 'u geoclue - "Geoinformation service" /var/lib/geoclue' > "${DESTDIR}/usr/lib/sysusers.d/geoclue.conf"
chmod 644 "${DESTDIR}/usr/lib/sysusers.d/geoclue.conf"

install -dm644 "${DESTDIR}/usr/lib/tmpfiles.d/"
echo 'd /var/lib/geoclue 0755 geoclue geoclue'		     > "${DESTDIR}/usr/lib/tmpfiles.d/geoclue.conf"
chmod 644 "${DESTDIR}/usr/lib/tmpfiles.d/geoclue.conf"
