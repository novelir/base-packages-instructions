#!/bin/sh -e

#unset CC
#unset CXX

./configure \
    gt_cv_func_printf_posix=yes \
    --prefix=/usr \
    --bindir=/usr/bin \
    --sbindir=/usr/bin \
    --libexecdir=/usr/lib \
    --datarootdir=/usr/share \
    --datadir=/usr/share \
    --sysconfdir=/var/etc \
    --localstatedir=/var \
    --includedir=/usr/include \
    --libdir=/usr/lib \
    --disable-openmp \
    --disable-java \
    --disable-csharp \
    --enable-acl \
    --enable-nls \
    --enable-static \
    --without-emacs \
    --without-included-gettext \
    --with-bzip2 \
    --with-xz

make
make -j1 DESTDIR="${DESTDIR}" install

