#!/bin/sh -e

export PATH="/usr/bin/python2:$PATH"
export PATH="/usr/bin/qt5:$PATH"

export CFLAGS="$CFLAGS -fno-lto"
export CXXFLAGS="$CXXFLAGS -fno-lto"
export LDFLAGS="$LDFLAGS -fno-lto"
export NINJAFLAGS="$MAKEFLAGS"

# Disable jumbo build
sed -e '/use_jumbo_build=true/s/true/false/' -i src/buildtools/config/common.pri

# Fix gn build with LLVM
sed -i 's/gn_gen_args =/gn_gen_args = --no-static-libstdc++/g' src/buildtools/gn.pro

# Fix text rendering when building with glibc 2.33
patch -p1 -i qt5-webengine-glibc-2.33.patch

# Fix Aarch64
if [ $PILE_ARCH = aarch64 ]
then
  cd src/3rdparty
    patch -p1 -i ../../0001-AARCH64-toolchain-fix.patch
    patch -p1 -i ../../0002-Fix-ARM-skia-ICE.patch
    patch -p1 -i ../../0003-bind-gen-Support-single_process-flag-in-generate_bin.patch
    patch -p1 -i ../../0004-Run-blink-bindings-generation-single-threaded.patch
    patch -p1 -i ../../0005-Fix-sandbox-Aw-snap-for-sycalls-403-and-407.patch
  cd ../..
fi


mkdir ../qt5-qtwebengine.build
cd ../qt5-qtwebengine.build

qmake ../qt5-qtwebengine -- \
    -no-feature-webengine-system-glib \
    -feature-webengine-system-libxml2 \
    -no-webengine-pulseaudio \
    -no-webengine-embedded-build \
    -no-webengine-webrtc \
    -no-webengine-kerberos \
    -webengine-alsa \
    -webengine-icu \
    -webengine-ffmpeg \
    -webengine-opus \
    -webengine-webp \
    -webengine-pepper-plugins \
    -webengine-printing-and-pdf \
    -webengine-proprietary-codecs \
    -webengine-spellchecker \
    -webengine-extensions

make
make INSTALL_ROOT="${DESTDIR}" install

# Fix QtWebengine version name
sed -e "s|$(git describe --tags --abbrev=0 | sed -e '1s/^.//' -e 's/\-.*//')\ |5.15.2 |" -i "${DESTDIR}/usr/lib/cmake/Qt5*/*Config.cmake"

# Drop QMAKE_PRL_BUILD_DIR because reference the build dir
find "${DESTDIR}/usr/lib" -type f -name '*.prl' \
   -exec sed -i -e '/^QMAKE_PRL_BUILD_DIR/d' {} \;

