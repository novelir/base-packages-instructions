#!/bin/sh -e

./configure \
    ac_cv_path_PERL="echo" \
    --prefix=/usr \
    --bindir=/usr/bin \
    --sbindir=/usr/bin \
    --libexecdir=/usr/lib \
    --datarootdir=/usr/share \
    --datadir=/usr/share \
    --sysconfdir=/var/etc \
    --localstatedir=/var \
    --runstatedir=/run \
    --includedir=/usr/include \
    --libdir=/usr/lib \
    --disable-manual \
    --disable-ldap \
    --disable-ldaps \
    --enable-versioned-symbols \
    --enable-threaded-resolver \
    --enable-ipv6 \
    --enable-static \
    --without-libidn \
    --without-libidn2 \
    --without-librtmp \
    --without-libpsl \
    --without-libssh2 \
    --with-pic \
    --with-icu \
    --with-random=/dev/urandom \
    --with-ca-bundle=/etc/ssl/certs/ca-certificates.crt

make
make DESTDIR="${DESTDIR}" install
