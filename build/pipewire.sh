#!/bin/sh -e

setup-meson \
    --prefix=/usr \
    -Ddocs=disabled \
    -Dexamples=enabled \
    -Dmedia-session=enabled \
    -Dman=disabled \
    -Dgstreamer=enabled \
    -Dgstreamer-device-provider=enabled \
    -Dsystemd=enabled \
    -Dsystemd-system-service=disabled \
    -Dsystemd-user-service=enabled \
    -Dpipewire-alsa=enabled \
    -Dpipewire-jack=enabled \
    -Dspa-plugins=enabled \
    -Dalsa=enabled \
    -Daudiomixer=enabled \
    -Daudioconvert=enabled \
    -Dbluez5=enabled \
    -Dbluez5-backend-hsp-native=enabled \
    -Dbluez5-backend-hfp-native=enabled \
    -Dbluez5-backend-ofono=enabled \
    -Dbluez5-backend-hsphfpd=enabled \
    -Dbluez5-codec-aptx=disabled \
    -Dbluez5-codec-ldac=disabled \
    -Dbluez5-codec-aac=disabled \
    -Dcontrol=enabled \
    -Dffmpeg=disabled \
    -Djack=disabled \
    -Dsupport=enabled \
    -Devl=disabled \
    -Dv4l2=enabled \
    -Dlibcamera=disabled \
    -Dvideoconvert=enabled \
    -Dvolume=enabled \
    -Dvulkan=enabled \
    -Dudev=enabled \
    . build

meson compile -C build
meson install -C build


mkdir -p "${DESTDIR}"/var/etc/alsa/conf.d
ln -sf ../../../../usr/share/alsa/alsa.conf.d/50-pipewire.conf          "${DESTDIR}"/var/etc/alsa/conf.d/
ln -sf ../../../../usr/share/alsa/alsa.conf.d/99-pipewire-default.conf  "${DESTDIR}"/var/etc/alsa/conf.d/

