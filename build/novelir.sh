#!/bin/sh -e

chmod    644 usr/share/mime/packages
chmod -R 644 var/etc/*
chmod    600 var/etc/crypttab

cp -rpf usr "${DESTDIR}/"
cp -rpf var "${DESTDIR}/"

