#!/bin/sh -e

./configure \
    --prefix=/usr \
    --bindir=/usr/bin \
    --sbindir=/usr/bin \
    --libexecdir=/usr/lib \
    --datarootdir=/usr/share \
    --datadir=/usr/share \
    --sysconfdir=/var/etc \
    --localstatedir=/var \
    --runstatedir=/run \
    --includedir=/usr/include \
    --libdir=/usr/lib

make
make DESTDIR="${DESTDIR}" install

