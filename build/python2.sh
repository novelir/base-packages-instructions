#!/bin/sh -e

./configure \
    --prefix=/usr \
    --bindir=/usr/bin/python2 \
    --sbindir=/usr/bin/python2 \
    --libexecdir=/usr/lib \
    --datarootdir=/usr/share \
    --datadir=/usr/share \
    --sysconfdir=/var/etc \
    --localstatedir=/var \
    --includedir=/usr/include \
    --libdir=/usr/lib \
    --disable-profiling \
    --disable-optimizations \
    --enable-shared \
    --enable-ipv6 \
    --enable-unicode=ucs4 \
    --without-gcc \
    --without-doc-strings \
    --without-ensurepip \
    --with-lto \
    --with-system-ffi \
    --with-system-expat

make
make DESTDIR="${DESTDIR}" install

