#!/bin/sh -e

for FLAG in -Wl,--as-needed -Wl,-z,now -Wl,-z,relro
do
  export CFLAGS="$(echo $CFLAGS | sed "s/$FLAG/ /g")"
  export CXXFLAGS="$(echo $CXXFLAGS | sed "s/$FLAG/ /g")"
  export LDFLAGS="$(echo $LDFLAGS | sed "s/$FLAG/ /g")"
done

./configure \
    ac_cv_path_PERL="echo" \
    --prefix=/usr \
    --bindir=/usr/bin \
    --sbindir=/usr/bin \
    --libexecdir=/usr/lib \
    --datarootdir=/usr/share \
    --datadir=/usr/share \
    --sysconfdir=/var/etc \
    --localstatedir=/var \
    --runstatedir=/run \
    --includedir=/usr/include \
    --libdir=/usr/lib \
    --disable-assert \
    --disable-gcc-warnings \
    --enable-nls

make
make "${DESTDIR}" install

