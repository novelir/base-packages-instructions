#!/bin/sh -e

cat > "${DESTDIR}/usr/bin/makeinfo" <<EOF
#!/bin/sh
echo "SKIPPED : makeinfo"
EOF
chmod +x "${DESTDIR}/usr/bin/makeinfo"


cat > "${DESTDIR}/usr/bin/help2man" <<EOF
#!/bin/sh
echo "SKIPPED : help2man"
EOF
chmod +x "${DESTDIR}/usr/bin/help2man"


cat > "${DESTDIR}/usr/bin/autoconf" <<EOF
#!/bin/sh
echo "SKIPPED : autoconf"
EOF
chmod +x "${DESTDIR}/usr/bin/autoconf"
ln -sf autoconf "${DESTDIR}/usr/bin/autoconf-1.15"
ln -sf autoconf "${DESTDIR}/usr/bin/autoconf-1.16"


cat > "${DESTDIR}/usr/bin/autopoint" <<EOF
#!/bin/sh
echo "SKIPPED : autopoint"
EOF
chmod +x "${DESTDIR}/usr/bin/autopoint"
ln -sf autopoint "${DESTDIR}/usr/bin/autopoint-1.15"
ln -sf autopoint "${DESTDIR}/usr/bin/autopoint-1.16"


cat > "${DESTDIR}/usr/bin/autoheader" <<EOF
#!/bin/sh
echo "SKIPPED : autoheader"
EOF
chmod +x "${DESTDIR}/usr/bin/autoheader"
ln -sf autoheader "${DESTDIR}/usr/bin/autoheader-1.15"
ln -sf autoheader "${DESTDIR}/usr/bin/autoheader-1.16"


cat > "${DESTDIR}/usr/bin/automake" <<EOF
#!/bin/sh
echo "SKIPPED : automake"
EOF
chmod +x "${DESTDIR}/usr/bin/automake"
ln -sf automake "${DESTDIR}/usr/bin/automake-1.15"
ln -sf automake "${DESTDIR}/usr/bin/automake-1.16"


cat > "${DESTDIR}/usr/bin/aclocal" <<EOF
#!/bin/sh
echo "SKIPPED : aclocal"
EOF
chmod +x "${DESTDIR}/usr/bin/aclocal"
ln -sf aclocal "${DESTDIR}/usr/bin/aclocal-1.15"
ln -sf aclocal "${DESTDIR}/usr/bin/aclocal-1.16"
