#!/bin/sh -e

export CFLAGS="$CFLAGS -fno-lto"
export CXXFLAGS="$CXXFLAGS -fno-lto"
export LDFLAGS="$LDFLAGS -fno-lto"

# Use /usr/lib
sed -i '/m64=/s/lib64/lib/' gcc/config/i386/t-linux64

rm -rf ../gcc.build
mkdir -p ../gcc.build
cd ../gcc.build

../gcc/configure \
    --prefix=/usr \
    --bindir=/usr/bin \
    --sbindir=/usr/bin \
    --libexecdir=/usr/lib \
    --datarootdir=/usr/share \
    --datadir=/usr/share \
    --sysconfdir=/var/etc \
    --localstatedir=/var \
    --includedir=/usr/include \
    --libdir=/usr/lib \
    --disable-werror \
    --disable-multilib \
    --disable-multiarch \
    --disable-bootstrap \
    --enable-default-pie \
    --enable-default-ssp \
    --enable-cet=auto \
    --enable-lto \
    --enable-nls \
    --enable-plugin \
    --enable-threads \
    --enable-shared \
    --enable-languages=c,c++,lto \
    --with-system-zlib \
    --with-isl \
    --with-pic \
    --with-linker-hash-style=gnu

make
make DESTDIR="${DESTDIR}" install


ln -sf gcc "${DESTDIR}"/usr/bin/cc


mkdir -p _files
cat > _files/c99 <<EOF
#!/bin/sh
exec cc -std=c99 "$@"
EOF
install -Dm755 _files/c99 "${DESTDIR}/bin/c99"


install -v -dm755 "${DESTDIR}"/lib/bfd-plugins
ln -sf "/usr/lib/gcc/$(${DESTDIR}/usr/bin/gcc -dumpmachine)/$(${DESTDIR}/usr/bin/gcc -dumpversion)/liblto_plugin.so" "${DESTDIR}"/usr/lib/bfd-plugins/

