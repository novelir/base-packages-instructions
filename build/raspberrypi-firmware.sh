#!/bin/sh -e

mkdir -p "${DESTDIR}/usr/lib/dtbs"

mv firmware-*/boot/bootcode.bin  "${DESTDIR}/usr/lib/dtbs/"
mv firmware-*/boot/fixup*.elf    "${DESTDIR}/usr/lib/dtbs/"
mv firmware-*/boot/start*.elf    "${DESTDIR}/usr/lib/dtbs/"
mv firmware-*/boot/overlays      "${DESTDIR}/usr/lib/dtbs/"

