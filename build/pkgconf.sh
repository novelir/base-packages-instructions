#!/bin/sh -e

setup-meson \
    --prefix=/usr \
    -Dtests=false \
    . build

meson compile -C build
meson install -C build


ln -sf pkgconf "${DESTDIR}"/bin/pkg-config

