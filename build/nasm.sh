#!/bin/sh -e

unset MAKEFLAGS

./configure \
    --prefix=/usr \
    --bindir=/usr/bin \
    --sbindir=/usr/bin \
    --libexecdir=/usr/lib \
    --datarootdir=/usr/share \
    --datadir=/usr/share \
    --sysconfdir=/var/etc \
    --localstatedir=/var \
    --includedir=/usr/include \
    --libdir=/usr/lib \
    --disable-suggestions \
    --disable-werror \
    --disable-pedantic

make
make DESTDIR="${DESTDIR}" install
