#!/bin/sh -e

./configure \
    --prefix=/usr \
    --bindir=/usr/bin \
    --sbindir=/usr/bin \
    --libexecdir=/usr/lib \
    --datarootdir=/usr/share \
    --datadir=/usr/share \
    --sysconfdir=/var/etc \
    --localstatedir=/var \
    --includedir=/usr/include \
    --libdir=/usr/lib \
    --with-pic

make
make DESTDIR="${DESTDIR}" install

# Maintain compatibility and avoid the need
# for rebuilds of all packages linking to
# libffi.
#
# ABI incompatibility only affects AArch64.
# See: https://github.com/libffi/libffi/commit/c02c341
#      https://github.com/libffi/libffi/issues/528
ln -sf libffi.so.7 "${DESTDIR}"/lib/libffi.so.6

