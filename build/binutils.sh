#!/bin/sh -e

./configure \
    --prefix=/usr \
    --bindir=/usr/bin/binutils \
    --sbindir=/usr/bin/binutils \
    --libexecdir=/usr/lib \
    --datarootdir=/usr/share \
    --datadir=/usr/share \
    --sysconfdir=/var/etc \
    --localstatedir=/var \
    --includedir=/usr/include \
    --libdir=/usr/lib \
    --program-prefix='' \
    --disable-werror \
    --disable-multilib \
    --disable-libssp \
    --disable-gdb \
    --disable-readline \
    --disable-gprof \
    --disable-gold \
    --enable-nls \
    --enable-ld=default \
    --enable-64-bit-bfd \
    --enable-64-bit-archive \
    --enable-shared \
    --enable-threads \
    --enable-relro \
    --enable-new-dtags \
    --enable-cet \
    --enable-lto \
    --enable-plugins \
    --enable-default-hash-style=gnu \
    --enable-deterministic-archives \
    --with-system-zlib \
    --with-pic \
    --with-mmap

make prefix="/usr" tooldir="/usr/tooldir"
make DESTDIR="${DESTDIR}" prefix="/usr" tooldir="/usr/tooldir" install

cp -r "${DESTDIR}/usr/tooldir/lib" "${DESTDIR}/usr/"
rm -rf "${DESTDIR}/usr/tooldir"
