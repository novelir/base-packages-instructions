#!/bin/sh -e

./configure \
    --prefix=/usr \
    --bindir=/usr/bin \
    --sbindir=/usr/bin \
    --libexecdir=/usr/lib/iwd \
    --datarootdir=/usr/share \
    --datadir=/usr/share \
    --sysconfdir=/var/etc \
    --localstatedir=/var \
    --runstatedir=/run \
    --includedir=/usr/include \
    --libdir=/usr/lib \
    --disable-manual-pages \
    --disable-tools \
    --disable-debug \
    --enable-systemd-service \
    --enable-client \
    --enable-pie \
    --enable-wired \
    --enable-hwsim \
    --enable-ofono

make
make DESTDIR="${DESTDIR}" install
