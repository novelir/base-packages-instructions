#!/bin/sh -e

setup-meson \
    --prefix=/usr \
    -Dlibmount=disabled \
    -Dinstalled_tests=false \
    -Ddefault_library=both \
    -Dgtk_doc=false \
    -Dman=false \
    -Dfam=false \
    -Dtests=false \
    -Dglib_checks=false \
    -Dnls=disabled \
    -Dinternal_pcre=true \
    . build

meson compile -C build
meson install -C build

