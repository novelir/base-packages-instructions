#!/bin/sh -e

./configure \
    --prefix=/usr \
    --bindir=/usr/bin \
    --sbindir=/usr/bin \
    --libexecdir=/usr/lib \
    --datarootdir=/usr/share \
    --datadir=/usr/share \
    --sysconfdir=/var/etc \
    --localstatedir=/var \
    --runstatedir=/run \
    --includedir=/usr/include \
    --libdir=/usr/lib \
    --enable-systemd \
    --disable-debug \
    --disable-test \
    --disable-monitor \
    --disable-deprecated \
    --disable-manpages \
    --disable-testing \
    --disable-mesh \
    --enable-client \
    --enable-pie \
    --enable-library \
    --enable-nfc \
    --enable-cups \
    --enable-sixaxis

make
make DESTDIR="${DESTDIR}" install
