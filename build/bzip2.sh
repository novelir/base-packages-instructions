#!/bin/sh -e

make CC="${CC:-cc} $CFLAGS" -f Makefile-libbz2_so
make CC="${CC:-cc} $CFLAGS" bzip2recover libbz2.a


for bin in bzip2-shared bzdiff bzgrep bzip2recover bzmore
do
  install -Dm755 "$bin" "${DESTDIR}"/usr/bin/"${bin%%-shared}"
done

install -Dm755 libbz2.so.1.0.8 "${DESTDIR}"/usr/lib/libbz2.so.1.0.8
install -Dm644 libbz2.a        "${DESTDIR}"/usr/lib/libbz2.a
install -Dm644 bzlib.h         "${DESTDIR}"/usr/include/bzlib.h


for lib in libbz2.so libbz2.so.1 libbz2.so.1.0; do
  ln -sf libbz2.so.1.0.8 "${DESTDIR}"/usr/lib/$lib
done

ln -sf bzip2 "${DESTDIR}"/usr/bin/bunzip2
ln -sf bzip2 "${DESTDIR}"/usr/bin/bzcat

