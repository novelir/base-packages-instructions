#!/bin/sh -e

./configure \
    --prefix=/usr \
    --bindir=/usr/bin \
    --sbindir=/usr/bin \
    --libexecdir=/usr/lib \
    --datarootdir=/usr/share \
    --datadir=/usr/share \
    --sysconfdir=/var/etc \
    --localstatedir=/var \
    --runstatedir=/run \
    --includedir=/usr/include \
    --libdir=/usr/lib \
    --mandir=/usr/share/man \
    --disable-rpath-hack \
    --disable-stripping \
    --enable-pc-files \
    --enable-widec \
    --without-ada \
    --without-tests \
    --without-debug \
    --without-cxx-binding \
    --with-pkg-config-libdir=/usr/lib/pkgconfig \
    --with-shared

make
make DESTDIR="${DESTDIR}" install

for lib in ncurses form panel menu
do
  rm -f "${DESTDIR}"/usr/lib/lib${lib}.so
  printf '%s\n' "INPUT(-l${lib}w)" > "${DESTDIR}"/usr/lib/lib${lib}.so
  chmod 755 "${DESTDIR}"/usr/lib/lib${lib}.so
  ln -sf "lib${lib}w.a" "${DESTDIR}"/usr/lib/lib${lib}.a
done

# Some packages look for libcurses instead of libncurses when building.
printf '%s\n' "INPUT(-lncursesw)" > "${DESTDIR}"/usr/lib/libcursesw.so
ln -sf libncurses.so "${DESTDIR}"/usr/lib/libcurses.so

# Fix pkgconfig file.
ln -sf ncursesw.pc "${DESTDIR}"/usr/lib/pkgconfig/ncurses.pc

