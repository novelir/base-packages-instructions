#!/bin/sh -e

mkdir build
cd build

../configure \
    --prefix=/usr \
    --bindir=/usr/bin \
    --sbindir=/usr/bin \
    --libexecdir=/usr/lib \
    --datarootdir=/usr/share \
    --datadir=/usr/share \
    --sysconfdir=/var/etc \
    --localstatedir=/var \
    --runstatedir=/run \
    --includedir=/usr/include \
    --libdir=/usr/lib \
    --disable-mpfr \
    --without-libsigsegv

make
make DESTDIR="${DESTDIR}" install

ln -sf gawk "${DESTDIR}"/usr/bin/awk

