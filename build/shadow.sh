#!/bin/sh -e

./configure \
    --prefix=/usr \
    --bindir=/usr/bin \
    --sbindir=/usr/bin \
    --libexecdir=/usr/lib \
    --datarootdir=/usr/share \
    --datadir=/usr/share \
    --sysconfdir=/var/etc \
    --localstatedir=/var \
    --runstatedir=/run \
    --includedir=/usr/include \
    --libdir=/usr/lib \
    --mandir=/usr/share/man \
    --enable-nls \
    --disable-man \
    --disable-rpath \
    --with-libpam \
    --with-group-name-max-length=32 \
    --without-audit \
    --without-selinux \
    --without-acl

make
make DESTDIR="${DESTDIR}" install


# Install pam files
for file in login passwd su
do
  install -Dm644 "$file.pam"	"${DESTDIR}/var/etc/pam.d/$file"
done
for file in chage chfn chgpasswd chpasswd chsh groupadd groupdel groupmems groupmod newusers useradd userdel usermod
do
  install -Dm644 default.pam	"${DESTDIR}/var/etc/pam.d/$file"
done


# Do not create mail directory
sed -e '/CREATE_MAIL_SPOOL/s/yes/no/'   -i "${DESTDIR}/var/etc/default/useradd"

# Change default umask
sed -e '/UMASK/s/022/027/'              -i "${DESTDIR}/var/etc/login.defs"

# Use SHA512 password encryption
sed -e '/#ENCRYPT_METHOD/s/#//' \
    -e '/#ENCRYPT_METHOD/s/DES/SHA512/' -i "${DESTDIR}/var/etc/login.defs"

# Enable /etc/issue
sed -e '/#ISSUE_FILE/s/#//'             -i "${DESTDIR}/var/etc/login.defs"

