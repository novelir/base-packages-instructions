#!/bin/sh -e

rm -rf ../glibc.build
mkdir -p ../glibc.build
cd ../glibc.build

case "$PILE_ARCH" in
  x86_64)
    export optparameters="--enable-cet --enable-static-pie"
    ;;
esac

CC="gcc" ../glibc/configure \
    ac_cv_path_PERL="no" \
    --prefix=/usr \
    --bindir=/usr/bin \
    --sbindir=/usr/bin \
    --libexecdir=/usr/lib \
    --datarootdir=/usr/share \
    --datadir=/usr/share \
    --sysconfdir=/var/etc \
    --localstatedir=/var \
    --includedir=/usr/include \
    --libdir=/usr/lib \
    --disable-werror \
    --disable-profile \
    --disable-multi-arch \
    --disable-build-nscd \
    --disable-nscd \
    --enable-lock-elision \
    --enable-add-ons \
    --enable-bind-now \
    --enable-kernel=5.11 \
    --enable-stackguard-randomization \
    --without-selinux \
    --with-headers=/usr/include \
    libc_cv_slibdir=/usr/lib \
    $optparameters

CC="gcc" make
CC="gcc" make DESTDIR=${DESTDIR} install


# Make ldd compatible with POSIX sh
sed -i 's/bash/sh/' \
    -i 's/\$\"/\"/' "${DESTDIR}/usr/bin/ldd"


rm -f "${DESTDIR}/var/etc/ld.so.cache"
mkdir -p "${DESTDIR}/var/etc/ld.so.conf.d"
cat > "${DESTDIR}/var/etc/ld.so.conf" <<EOF
# Dynamic linker/loader configuration.
# See ld.so(8) and ldconfig(8) for details.

include /var/etc/ld.so.conf.d/*.conf
EOF


{
  rm   -rf "${DESTDIR}"/usr/lib/locale
  mkdir -p "${DESTDIR}"/usr/lib/locale
  
  install -m755 ../glibc/locale-gen "${DESTDIR}/usr/bin"
  install -m644 ../glibc/locale.gen "${DESTDIR}/var/etc"
  sed -e '1,3d' -e 's|/| |g' -e 's|\\| |g' -e 's|^|#|g' \
      "../glibc/localedata/SUPPORTED" >> "${DESTDIR}/var/etc/locale.gen"
}


{
  cd ../glibc/tz
  sed -i "s:sbin:bin:g" Makefile
  
  timezones="africa antarctica asia australasia europe northamerica southamerica etcetera backward factory"

  make TZDIR="/usr/share/zoneinfo"
  make DESTDIR=${DESTDIR} install

  ./zic -b fat -d "${DESTDIR}"/usr/share/zoneinfo ${timezones}
  ./zic -b fat -d "${DESTDIR}"/usr/share/zoneinfo/posix ${timezones}
  ./zic -b fat -d "${DESTDIR}"/usr/share/zoneinfo/right -L leapseconds ${timezones}

  ./zic -b fat -d "${DESTDIR}"/usr/share/zoneinfo -p America/New_York
  install -m444 -t "${DESTDIR}"/usr/share/zoneinfo iso3166.tab zone1970.tab zone.tab
  
  rm ${DESTDIR}/usr/bin/tzselect
}

