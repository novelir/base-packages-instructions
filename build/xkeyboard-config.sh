#!/bin/sh -e

setup-meson \
    --prefix=/usr \
    . build

meson compile -C build
meson install -C build
