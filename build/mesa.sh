#!/bin/sh -e

export CFLAGS="$CFLAGS -fno-lto"
export CXXFLAGS="$CXXFLAGS -fno-lto"
export LDFLAGS="$LDFLAGS -fno-lto"

{
    cd mako

    python setup.py build
    python setup.py install \
        --prefix=/usr \
        --root="$PWD/dist"

    # Use a glob to avoid having to figure out the Python
    # version for the path below.
    cd dist/usr/lib/python*/site-packages

    # Set the PYTHONPATH so python knows where to find mako.
    # The one liner simply appends the existing path and
    # handles the case where an unset PYTHONPATH breaks
    # python as it will only contain our new addition.
    PYTHONPATH=$PWD:$(python -c "import sys; print(':'.join(sys.path))")

    cd -; cd ..
}

export PYTHONPATH

python3 bin/git_sha1_gen.py --output include/git_sha1.h

_gallium_drivers="radeonsi,nouveau,zink,virgl"
_vulkan_drivers="amd"
case "$PILE_ARCH" in
x86_64)
  _gallium_drivers="${_gallium_drivers},iris"
  _vulkan_drivers="${_vulkan_drivers},intel"
  ;;
aarch64|riscv64)
  _gallium_drivers="${_gallium_drivers},kmsro,freedreno,v3d,vc4,etnaviv,tegra,panfrost,lima"
  _vulkan_drivers="${_vulkan_drivers},broadcom,freedreno"
  ;;
esac

setup-meson \
    --prefix=/usr \
    -Dplatforms=wayland \
    -Dzstd=enabled \
    -Dvalgrind=disabled \
    -Dlibunwind=disabled \
    -Dllvm=enabled \
    -Dshared-llvm=enabled \
    -Dshader-cache=enabled \
    -Dshader-cache-default=true \
    -Dgbm=enabled \
    -Dglx=disabled \
    -Dglvnd=false \
    -Dshared-glapi=enabled \
    -Dgles1=disabled \
    -Dgles2=enabled \
    -Dopengl=true \
    -Degl=enabled \
    -Dsse2=false \
    -Ddri-drivers="${_dri_drivers:''}" \
    -Ddri3=disabled \
    -Dvulkan-drivers="${_vulkan_drivers:-'auto'}" \
    -Dvulkan-overlay-layer=true \
    -Dvulkan-device-select-layer=true \
    -Dgallium-drivers="${_gallium_drivers:-'auto'}" \
    -Dgallium-vdpau=disabled \
    -Dgallium-va=enabled \
    -Dgallium-xa=disabled \
    -Dgallium-nine=false \
    -Dgallium-xvmc=disabled \
    . build

meson compile -C build
meson install -C build

