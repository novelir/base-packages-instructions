#!/bin/sh -e

export CFLAGS="$CFLAGS -fno-lto"
export CXXFLAGS="$CXXFLAGS -fno-lto"
export LDFLAGS="$LDFLAGS -fno-lto"

case "$PILE_ARCH" in
    aarch64*)   _target="linux-aarch64" ;;
    riscv64*)   _target="linux-generic64" ;;
    x86_64)     _target="linux-x86_64-clang" ;;
esac

./Configure "${_target}" \
        --prefix=/usr \
	--libdir=lib \
	--openssldir=/var/etc/ssl \
	shared \
	no-comp no-zlib \
	no-ssl3-method \
	no-weak-ssl-ciphers \
	enable-ec_nistp_64_gcc_128 \
	$CPPFLAGS $CFLAGS $LDFLAGS -Wa,--noexecstack

make
make DESTDIR="${DESTDIR}" install

