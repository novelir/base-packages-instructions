#!/bin/sh -e

./configure \
    --prefix=/usr \
    --bindir=/usr/bin \
    --sbindir=/usr/bin \
    --libexecdir=/usr/lib \
    --datarootdir=/usr/share \
    --datadir=/usr/share \
    --sysconfdir=/var/etc \
    --localstatedir=/var \
    --runstatedir=/run \
    --includedir=/usr/include \
    --libdir=/usr/lib \
    --with-runstatedir=/run \
    --with-sysusersdir=/usr/lib/sysusers.d \
    --with-tmpfilesdir=/usr/lib/tmpfiles.d \
    --with-udevrulesprefix=60- \
    --disable-doxygen-doc \
    --enable-debug=no

make
make DESTDIR="${DESTDIR}" install
