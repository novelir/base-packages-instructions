#!/bin/sh -e

for FLAG in -Wl,--as-needed -Wl,-z,now -Wl,-z,relro
do
  export CFLAGS="$(echo $CFLAGS | sed "s/$FLAG/ /g")"
  export CXXFLAGS="$(echo $CXXFLAGS | sed "s/$FLAG/ /g")"
  export LDFLAGS="$(echo $LDFLAGS | sed "s/$FLAG/ /g")"
done

setup-meson \
    --prefix=/usr \
    -Ddebug-gui=false \
    -Ddocumentation=false \
    -Dtests=false \
    -Dlibwacom=false \
    . build

meson compile -C build
meson install -C build

