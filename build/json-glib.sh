#!/bin/sh -e

setup-meson \
    --prefix=/usr \
    -Dintrospection=disabled \
    -Dgtk_doc=disabled \
    -Dman=false \
    -Dtests=false \
    . build

meson compile -C build
meson install -C build

