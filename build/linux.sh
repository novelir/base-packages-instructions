#!/bin/sh -e

export MAKE_ARGS="LLVM=1 LLVM_IAS=1"

case $PILE_ARCH in
  aarch64)
    export MAKE_ARGS="$MAKE_ARGS ARCH=arm64"
    export IMAGE_TYPE="Image.gz"
    ;;
  riscv64)
    export MAKE_ARGS="$MAKE_ARGS ARCH=riscv"
    export IMAGE_TYPE="Image.gz"
    ;;
  x86_64)
    export MAKE_ARGS="$MAKE_ARGS ARCH=x86_64"
    export IMAGE_TYPE="bzImage"
    ;;
esac

make mrproper
mv "config.${PILE_ARCH}" .config
make ${MAKE_ARGS} olddefconfig

make ${MAKE_ARGS} \
    modules \
    ${IMAGE_TYPE} \
    headers


# Install modules
{
  mkdir -p "${DESTDIR}"/usr/lib/modules
  make ${MAKE_ARGS} INSTALL_MOD_PATH="${DESTDIR}/usr" INSTALL_MOD_STRIP=1 modules_install
}

# Install kernel
{
  mkdir -p "${DESTDIR}/usr/lib/kernel"
  # kernel
  mv "$(make -s image_name)" "${DESTDIR}/usr/lib/kernel/${PILE_NAME}-kernel"
  # cmdline
  echo "quiet loglevel=3 systemd.show_status=false udev.log_level=3 vt.global_cursor_default=0" \
    "${DESTDIR}/usr/lib/kernel/${PILE_NAME}-cmdline"
}

# Install headers
{
  mkdir -p "${DESTDIR}/usr/include"
  find usr/include -name \*.h -type f | while read -r file
  do
    install -Dm644 "$file" "${DESTDIR}"/$file
  done
}

# Build and install dtbs
if [ "${PILE_ARCH}" = 'aarch64' ] || [ "${PILE_ARCH}" = 'riscv64' ]
then
  mkdir -p "${DESTDIR}"/usr/lib/dtbs
  make ${MAKE_ARGS} dtbs
  make ${MAKE_ARGS} INSTALL_DTBS_PATH="${DESTDIR}/usr/lib/dtbs" dtbs_install
fi

