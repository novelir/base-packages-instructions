#!/bin/sh -e

sed -i 's/ | \$(FLEX)$//' doc/Makefile.in

./configure \
    --prefix=/usr \
    --bindir=/usr/bin \
    --sbindir=/usr/bin \
    --libexecdir=/usr/lib \
    --datarootdir=/usr/share \
    --datadir=/usr/share \
    --sysconfdir=/var/etc \
    --localstatedir=/var \
    --runstatedir=/run \
    --includedir=/usr/include \
    --libdir=/usr/lib \
    ac_cv_func_malloc_0_nonnull=yes \
    ac_cv_func_realloc_0_nonnull=yes
    
make
make DESTDIR="${DESTDIR}" install

ln -sf flex "${DESTDIR}/bin/lex"
