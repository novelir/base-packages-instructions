#!/bin/sh -e

./configure \
    --prefix=/usr \
    --bindir=/usr/bin \
    --sbindir=/usr/bin \
    --libexecdir=/usr/lib/dbus-1.0 \
    --datarootdir=/usr/share \
    --datadir=/usr/share \
    --sysconfdir=/var/etc \
    --localstatedir=/var \
    --runstatedir=/run \
    --includedir=/usr/include \
    --libdir=/usr/lib \
    --with-console-auth-dir=/run/console \
    --with-system-pid-file=/run/dbus/pid \
    --with-system-socket=/run/dbus/system_bus_socket \
    --with-dbus-user=dbus \
    --without-x \
    --disable-Werror \
    --disable-verbose-mode \
    --disable-static \
    --disable-tests \
    --disable-checks \
    --disable-asserts \
    --disable-xml-docs \
    --disable-doxygen-docs \
    --disable-ducktype-docs \
    --enable-systemd \
    --enable-inotify \
    --enable-user-session

make
make DESTDIR="${DESTDIR}" install

cd dbus-broker
  setup-meson \
      --prefix=/usr \
      -Daudit=false \
      -Ddocs=false \
      -Dlauncher=true \
      -Dlinux-4-17=true \
      -Dreference-test=false \
      -Dselinux=false \
      -Dsystem-console-users=gdm,sddm,lightdm,lxdm \
      . build

  meson compile -C build
  meson install -C build
cd ..

ln -sf ../../etc/machine-id "${DESTDIR}"/var/lib/dbus


install -dm644 "${DESTDIR}/usr/lib/sysusers.d/"
echo 'u dbus 81 "System Message Bus"' > "${DESTDIR}/usr/lib/sysusers.d/dbus.conf"
chmod 644 "${DESTDIR}/usr/lib/sysusers.d/dbus.conf"

