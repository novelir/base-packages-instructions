#!/bin/sh -e

for FLAG in -Wl,--as-needed -Wl,--strip-all -Wl,-z,now -Wl,-z,relro
do
  export CFLAGS="$(echo $CFLAGS | sed "s/$FLAG/ /g")"
  export CXXFLAGS="$(echo $CXXFLAGS | sed "s/$FLAG/ /g")"
  export LDFLAGS="$(echo $LDFLAGS | sed "s/$FLAG/ /g")"
done

./configure \
    --prefix=/usr \
    --bindir=/usr/bin \
    --sbindir=/usr/bin \
    --libexecdir=/usr/lib \
    --datarootdir=/usr/share \
    --datadir=/usr/share \
    --sysconfdir=/var/etc \
    --localstatedir=/var \
    --includedir=/usr/include \
    --libdir=/usr/lib \
    --with-pic \
    --disable-gcov

make
make DESTDIR="${DESTDIR}" install

