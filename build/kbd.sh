#!/bin/sh -e

./configure \
    --prefix=/usr \
    --bindir=/usr/bin \
    --sbindir=/usr/bin \
    --libexecdir=/usr/lib \
    --datarootdir=/usr/share/kbd \
    --datadir=/usr/share/kbd \
    --sysconfdir=/var/etc \
    --localstatedir=/var \
    --includedir=/usr/include \
    --libdir=/usr/lib \
    --disable-werror \
    --disable-rpath \
    --disable-tests

make KEYCODES_PROGS=yes RESIZECONS_PROGS=yes
make KEYCODES_PROGS=yes RESIZECONS_PROGS=yes DESTDIR="${DESTDIR}" install

install -Dm644 vlock.pam "${DESTDIR}"/var/etc/pam.d/vlock
