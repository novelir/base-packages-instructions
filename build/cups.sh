#!/bin/sh -e

export CFLAGS="$CFLAGS -O1"
export CXXFLAGS="$CXXFLAGS -O1"
export LDFLAGS="$LDFLAGS -O1"

unset MAKEFLAGS

./configure \
    --prefix=/usr \
    --bindir=/usr/bin \
    --sbindir=/usr/bin \
    --libexecdir=/usr/lib \
    --datarootdir=/usr/share \
    --datadir=/usr/share \
    --sysconfdir=/var/etc \
    --localstatedir=/var \
    --includedir=/usr/include \
    --libdir=/usr/lib \
    --with-logdir=/var/log/cups \
    --without-rcdir \
    --with-cupsd-file-perm=0755 \
    --with-cups-user=209 \
    --with-cups-group=209 \
    --with-optim="$CFLAGS" \
    --disable-webif \
    --disable-gssapi \
    --enable-systemd \
    --disable-launchd \
    --disable-gnutls \
    --enable-pam=yes \
    --enable-ssl=yes \
    --enable-relro \
    --enable-dbus

make
make DESTDIR="${DESTDIR}" install



## https://github.com/archlinux/svntogit-packages/blob/packages/cups/trunk/PKGBUILD
install -D -m644 cups.logrotate    "${DESTDIR}/var/etc/logrotate.d/cups"
install -D -m644 cups.pam          "${DESTDIR}/var/etc/pam.d/cups"

chmod 755 "${DESTDIR}"/var/spool
chmod 755 "${DESTDIR}"/var/etc


# use cups group FS#36769
install -Dm644 cups.sysusers "${DESTDIR}/usr/lib/sysusers.d/cups.conf"
sed -i "s:#User 209:User 209:" "${DESTDIR}"/var/etc/cups/cups-files.conf
sed -i "s:#User 209:User 209:" "${DESTDIR}"/var/etc/cups/cups-files.conf.default
sed -i "s:#Group 209:Group 209:" "${DESTDIR}"/var/etc/cups/cups-files.conf
sed -i "s:#Group 209:Group 209:" "${DESTDIR}"/var/etc/cups/cups-files.conf.default


# install ssl directory where to store the certs, solves some samba issues
install -dm700 -g 209 "${DESTDIR}"/var/etc/cups/ssl


# install some more configuration files that will get filled by cupsd
touch "${DESTDIR}"/var/etc/cups/printers.conf
touch "${DESTDIR}"/var/etc/cups/classes.conf
touch "${DESTDIR}"/var/etc/cups/subscriptions.conf
chgrp -R 209 "${DESTDIR}"/var/etc/cups

