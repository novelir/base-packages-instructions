#!/bin/sh -e

export CFLAGS="$CFLAGS -fno-lto"
export CXXFLAGS="$CXXFLAGS -fno-lto"
export LDFLAGS="$LDFLAGS -fno-lto"

setup-meson \
    --prefix=/usr \
    -Ddocumentation=false \
    -Ddtd_validation=false \
    . build

meson compile -C build
meson install -C build
