#!/bin/sh -e

find . -name config.guess -type f | while read -r file
do
  wget https://git.savannah.gnu.org/cgit/config.git/plain/config.guess -qO $file
  chmod 755 $file
done

find . -name config.sub -type f | while read -r file
do
  wget https://git.savannah.gnu.org/cgit/config.git/plain/config.sub -qO $file
  chmod 755 $file
done
