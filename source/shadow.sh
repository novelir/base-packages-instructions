#!/bin/sh -e

wget https://github.com/shadow-maint/shadow/releases/download/4.8.1/shadow-4.8.1.tar.xz -qO -									| tar -xJ && mv shadow-4.8.1 shadow

for FILE in default.pam login.pam passwd.pam su.pam
do
  wget -q https://gitlab.com/novelir/base-packages-instructions/-/raw/master/source/shadow/$FILE
  mv $FILE shadow/
done

