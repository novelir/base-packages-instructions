#!/bin/sh -e

wget https://ftp.gnu.org/gnu/gcc/gcc-11.1.0/gcc-11.1.0.tar.gz -qO -												| tar -xz && mv gcc-11.1.0 gcc
wget https://ftp.gnu.org/gnu/gmp/gmp-6.2.1.tar.xz -qO -														| tar -xJ && mv gmp-6.2.1 gcc/gmp
wget https://ftp.gnu.org/gnu/mpfr/mpfr-4.1.0.tar.xz -qO -													| tar -xJ && mv mpfr-4.1.0 gcc/mpfr
wget https://ftp.gnu.org/gnu/mpc/mpc-1.2.1.tar.gz -qO -														| tar -xz && mv mpc-1.2.1 gcc/mpc
wget http://isl.gforge.inria.fr/isl-0.24.tar.xz -qO -														| tar -xJ && mv isl-0.24 gcc/isl
