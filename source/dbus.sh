#!/bin/sh -e

wget https://dbus.freedesktop.org/releases/dbus/dbus-1.12.20.tar.gz -qO -											| tar -xz && mv dbus-1.12.20 dbus

wget https://github.com/bus1/dbus-broker/releases/download/v28/dbus-broker-28.tar.xz -qO -									| tar -xJ && mv dbus-broker-28 dbus/dbus-broker

