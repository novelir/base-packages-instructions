#!/bin/sh -e

wget https://gitlab.com/novelir/base-packages-instructions/-/archive/master/base-packages-instructions-master.tar.bz2 -qO -					| tar -xj && mv base-packages-instructions-master novelir-base-packages-instructions

mv novelir-base-packages-instructions/source/novelir novelir

rm -rf novelir-base-packages-instructions

