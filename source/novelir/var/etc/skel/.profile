#
# ~/.profile
#

if [ "$(id -u)" = "0" ]
then
  _usercolor="1"
else
  _usercolor="4"
fi

[ $(dirname $(tty)) = "/dev/pts" ] && PS1='\[\033[0m\]\n\
\[\033[0;3'"${_usercolor}"'m\]◢\[\033[0;7;3'"${_usercolor}"'m\] \u \
\[\033[0;7;4'"${_usercolor}"';9'"${_usercolor}"'m\]◤\[\033[0;7;9'"${_usercolor}"'m\] \w \
\[\033[0;9'"${_usercolor}"'m\]◤\[\033[0m\]\n\
\[\033[2m\] \$\[\033[0m\] '

[ $(dirname $(tty)) = "/dev/pts" ] && PS2='\[\033[0;2m\] >\[\033[0m\] '

unset _usercolor
unset _rootcolor

