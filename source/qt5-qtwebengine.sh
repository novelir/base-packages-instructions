#!/bin/sh -e

rm -rf qtwebengine-chromium

git clone https://code.qt.io/qt/qtwebengine.git			qt5-qtwebengine
git clone https://code.qt.io/qt/qtwebengine-chromium.git	qtwebengine-chromium

cd qt5-qtwebengine
  git checkout v5.15.5-lts
  git submodule init
  git submodule set-url src/3rdparty ../qtwebengine-chromium
  git submodule set-branch --branch 87-based src/3rdparty
  git submodule update
cd ..

for FILE in 0001-AARCH64-toolchain-fix.patch 0002-Fix-ARM-skia-ICE.patch 0003-bind-gen-Support-single_process-flag-in-generate_bin.patch 0004-Run-blink-bindings-generation-single-threaded.patch 0005-Fix-sandbox-Aw-snap-for-sycalls-403-and-407.patch qt5-webengine-glibc-2.33.patch qtbug-91773.patch qtbug-93802.patch
do
  wget -q https://gitlab.com/novelir/base-packages-instructions/-/raw/master/source/qt5-qtwebengine/$FILE
  mv $FILE qt5-qtwebengine/
done

