#!/bin/sh -e

wget https://ftp.gnu.org/gnu/libc/glibc-2.33.tar.bz2 -qO -													| tar -xj && mv glibc-2.33 glibc


mkdir -p glibc/tz
cd glibc/tz
  wget https://data.iana.org/time-zones/releases/tzcode2021a.tar.gz -qO - | tar -xz
  wget https://data.iana.org/time-zones/releases/tzdata2021a.tar.gz -qO - | tar -xz
cd ../..


for FILE in locale.gen locale-gen
do
  wget -q https://gitlab.com/novelir/base-packages-instructions/-/raw/master/source/glibc/$FILE
  mv $FILE glibc/
done
