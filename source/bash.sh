#!/bin/sh -e

wget https://ftp.gnu.org/gnu/bash/bash-5.1.tar.gz -qO -														| tar -xz && mv bash-5.1 bash

for PATCH in 001 002 003 004 005 006 007 008
do
  curl -Os https://ftp.gnu.org/gnu/bash/bash-5.1-patches/bash51-$PATCH
  mv bash51-$PATCH bash/
done
