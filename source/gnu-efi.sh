#!/bin/sh -e

wget https://sourceforge.net/projects/gnu-efi/files/gnu-efi-3.0.13.tar.bz2 -qO -										| tar -xj && mv gnu-efi-3.0.13 gnu-efi

for FILE in gnu-efi-3.0.9-fix-clang-build.patch gnu-efi-3.0.10-fallthroug.patch
do
  wget -q https://gitlab.com/novelir/base-packages-instructions/-/raw/master/source/gnu-efi/$FILE
  mv $FILE gnu-efi/
done

