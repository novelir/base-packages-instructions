#!/bin/sh -e

mkdir -p fontconfig

wget https://download.savannah.gnu.org/releases/freetype/freetype-2.10.4.tar.xz -qO -										| tar -xJ && mv freetype-2.10.4 fontconfig/freetype
wget https://github.com/harfbuzz/harfbuzz/releases/download/2.8.1/harfbuzz-2.8.1.tar.xz -qO -									| tar -xJ && mv harfbuzz-2.8.1 fontconfig/harfbuzz
wget https://www.freedesktop.org/software/fontconfig/release/fontconfig-2.13.1.tar.gz -qO -									| tar -xz && mv fontconfig-2.13.1 fontconfig/fontconfig

