#!/bin/sh -e

mkdir -p roboto-mono-font

cd roboto-mono-font
  wget https://github.com/googlefonts/RobotoMono/raw/8f651634e746da6df6c2c0be73255721d24f2372/fonts/variable/RobotoMono%5Bwght%5D.ttf		-qO Roboto-Mono.ttf
  wget https://github.com/googlefonts/RobotoMono/raw/8f651634e746da6df6c2c0be73255721d24f2372/fonts/variable/RobotoMono-Italic%5Bwght%5D.ttf	-qO Roboto-MonoItalic.ttf
  wget https://apache.org/licenses/LICENSE-2.0.txt												-qO LICENSE.txt
cd ..

