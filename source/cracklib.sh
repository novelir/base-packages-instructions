#!/bin/sh -e

wget https://github.com/cracklib/cracklib/releases/download/v2.9.7/cracklib-2.9.7.tar.bz2 -qO -									| tar -xj && mv cracklib-2.9.7 cracklib

wget https://github.com/cracklib/cracklib/releases/download/v2.9.7/cracklib-words-2.9.7.bz2
mv cracklib-words-2.9.7.bz2 cracklib/cracklib-words.bz2
