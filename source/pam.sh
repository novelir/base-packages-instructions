#!/bin/sh -e

wget https://github.com/linux-pam/linux-pam/releases/download/v1.5.1/Linux-PAM-1.5.1.tar.xz -qO -								| tar -xJ && mv Linux-PAM-1.5.1 pam

mkdir -p pam/etc/pam.d
for FILE in other common-account common-auth common-password common-session system-remote-login system-local-login
do
  wget -q https://gitlab.com/novelir/base-packages-instructions/-/raw/master/source/pam/$FILE
  mv $FILE pam/etc/pam.d/
done
