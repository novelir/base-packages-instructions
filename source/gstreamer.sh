#!/bin/sh -e

wget https://gstreamer.freedesktop.org/src/gstreamer/gstreamer-1.18.3.tar.xz -qO -										| tar -xJ && mv gstreamer-1.18.3 gstreamer
wget https://gstreamer.freedesktop.org/src/gst-plugins-ugly/gst-plugins-ugly-1.18.3.tar.xz -qO -								| tar -xJ && mv gst-plugins-ugly-1.18.3 gstreamer/gst-plugins-ugly
wget https://gstreamer.freedesktop.org/src/gst-plugins-bad/gst-plugins-bad-1.18.3.tar.xz -qO -									| tar -xJ && mv gst-plugins-bad-1.18.3 gstreamer/gst-plugins-bad
wget https://gstreamer.freedesktop.org/src/gst-plugins-good/gst-plugins-good-1.18.3.tar.xz -qO -								| tar -xJ && mv gst-plugins-good-1.18.3 gstreamer/gst-plugins-good
wget https://gstreamer.freedesktop.org/src/gst-plugins-base/gst-plugins-base-1.18.3.tar.xz -qO -								| tar -xJ && mv gst-plugins-base-1.18.3 gstreamer/gst-plugins-base
