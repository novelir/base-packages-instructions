#!/bin/sh -e

wget https://github.com/rsms/inter/releases/download/v3.18/Inter-3.18.zip -qO inter-font.zip

unzip inter-font.zip -d inter-font.bundle
rm inter-font.zip

mv "inter-font.bundle/Inter Variable/Single axis" inter-font
mv "inter-font.bundle/LICENSE.txt" inter-font/
rm -r inter-font.bundle
