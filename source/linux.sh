#!/bin/sh -e

wget https://cdn.kernel.org/pub/linux/kernel/v5.x/linux-5.13.tar.xz -qO -											| tar -xJ && mv linux-5.13 linux

for FILE in config.aarch64 config.x86_64
do
  wget -q https://gitlab.com/novelir/base-packages-instructions/-/raw/master/source/linux/$FILE
  mv $FILE linux/
done

