#!/bin/sh -e

wget https://www.alsa-project.org/files/pub/lib/alsa-lib-1.2.5.tar.bz2 -qO -											| tar -xj && mv alsa-lib-1.2.5 alsa-lib

wget -q https://gitlab.com/novelir/base-packages-instructions/-/raw/master/source/alsa-lib/fix-dlo.patch
mv fix-dlo.patch alsa-lib/

