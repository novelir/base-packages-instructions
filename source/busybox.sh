#!/bin/sh -e

wget https://www.busybox.net/downloads/busybox-1.33.1.tar.bz2 -qO -												| tar -xj && mv busybox-1.33.1 busybox

for FILE in busybox.config fix-UB+clang.patch
do
  wget -q https://gitlab.com/novelir/base-packages-instructions/-/raw/master/source/busybox/$FILE
  mv $FILE busybox/
done

