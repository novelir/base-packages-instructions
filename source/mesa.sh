#!/bin/sh -e

wget https://archive.mesa3d.org/mesa-21.1.3.tar.xz --no-check-certificate -qO -											| tar -xJ && mv mesa-21.1.3 mesa

wget https://files.pythonhosted.org/packages/source/M/Mako/Mako-1.1.4.tar.gz -qO -										| tar -xz && mv Mako-1.1.4 mesa/mako
