#!/bin/sh -e

wget https://mirrors.edge.kernel.org/pub/linux/utils/kbd/kbd-2.4.0.tar.xz -qO -											| tar -xJ && mv kbd-2.4.0 kbd

wget -q https://gitlab.com/novelir/base-packages-instructions/-/raw/master/source/kbd/vlock.pam
mv vlock.pam kbd/
