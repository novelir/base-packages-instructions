#!/bin/sh -e

wget https://github.com/apple/cups/releases/download/v2.3.3/cups-2.3.3-source.tar.gz -qO -									| tar -xz && mv cups-2.3.3 cups

for FILE in cups.logrotate cups.pam cups.sysusers
do
  wget -q https://gitlab.com/novelir/base-packages-instructions/-/raw/master/source/cups/$FILE
  mv $FILE cups/
done
