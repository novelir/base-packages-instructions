#!/bin/sh -e

wget https://mirrors.edge.kernel.org/pub/linux/bluetooth/sbc-1.5.tar.xz -qO -											| tar -xJ && mv sbc-1.5 sbc

wget -q https://gitlab.com/novelir/base-packages-instructions/-/raw/master/source/sbc/fix-build-on-non-x86.patch
mv fix-build-on-non-x86.patch sbc/
