#!/bin/sh -e

wget https://download.samba.org/pub/ppp/ppp-2.4.9.tar.gz -qO -													| tar -xz && mv ppp-2.4.9 ppp

for FILE in ip-down ip-down.d.dns.sh ip-up ip-up.d.dns.sh ipv6-down ipv6-up ipv6-up.d.iface-config.sh options
do
  wget -q https://gitlab.com/novelir/base-packages-instructions/-/raw/master/source/ppp/$FILE
  mv $FILE ppp/
done

