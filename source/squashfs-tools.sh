#!/bin/sh -e

wget https://github.com/plougher/squashfs-tools/archive/refs/tags/4.4.tar.gz -qO -										| tar -xz && mv squashfs-tools-4.4 squashfs-tools

wget -q https://gitlab.com/novelir/base-packages-instructions/-/raw/master/source/squashfs-tools/0001-squashfs-tools-fix-build-failure-against-gcc-10.patch
mv 0001-squashfs-tools-fix-build-failure-against-gcc-10.patch squashfs-tools/
